# Redmine Clinet Tool
# Copyright (C) 2014 Fusion Systems Japan
#
# This program is a part of plugin called Charts Plugin of Redmine.
# This program generate invoicing issues on Redmine automatically.
#
# Auther: Hidekazu Inoue
#
require "./ws_client"
require "net/https"
require "uri"
require 'rexml/document'
require "date"
require 'rubygems'
require 'active_support/all'
require 'json'
require './sendmail'

USEAGE = <<-EOF
Useage
ruby invoice.rb <location> <delivery opiton> <weekly option> <test mode>

  location:
    This is mandatory option. You have to select a location to generate invoice
    from tk, hk, sh or la.

  delivery opiton:
    To generate invoice tickets for delivery, you set this opiton as delivery.
    Other arugument would be T&M.

  weekly option:
    Weekly invoice ticket -> yes
    Monthly invoice ticket -> no

  test mode
    Testing(localhost) -> yes
    Live -> no

e.g.)
ruby invoice.rb tk no no no

EOF

FINANCE_ISSUE_PROJECT_ID_TK = 813  # Tokyo Finace Project ID
FINANCE_ISSUE_PROJECT_ID_SH = 814  # Shnaghai Finace Project ID
FINANCE_ISSUE_PROJECT_ID_LA = 815  # LA Finace Project ID
FINANCE_ISSUE_PROJECT_ID_HK = 816  # HK Finace Project ID
PROJECT_ID_TK = "itms-financials"
PROJECT_ID_SH = "itms-financials-sh"
PROJECT_ID_LA = "itms-financials-la"
PROJECT_ID_HK = "itms"

LIMIT = 100 # Limmitaiton for the maximum data number of Redmine web service

class FinanceIssue
  attr_accessor :id, :subject, :tracker, :status, :sales, :start_date, :due_date, \
                :subject_origin, :prepaiduntil, :contractual_hours, :sow_type
  def initialize(values)
    @contractual_hours = 0
    values.each do |k, v|
      self.send("#{k}=", v)
    end
  end
end

class InvoiceMaker
  include SendMail

  def initialize(location="tk", delivery=false, weekly=false, test_mode=true)
    case location
    when "tk"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_TK
      @project_id = PROJECT_ID_TK
    when "sh"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_SH
      @project_id = PROJECT_ID_SH
    when "la"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_LA
      @project_id = PROJECT_ID_LA
    when "hk"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_HK
      @project_id = PROJECT_ID_HK
    else
      raise "oops! confirm arguments #{location}"
    end

    @ws_client = WsClient.new test_mode
    @test_mode = test_mode
    @today = Date.today
    @month_label = @today.strftime("%Y/%m")
    @week_label = (@today - 1.week).beginning_of_week.strftime("%Y@%U")
    @delivery = delivery
    @weekly = weekly

    @project_map = {}
    @log_buffer = ''
  end

  # Collect all finance tickets
  def grab_standing_finance_issues
    targets = []

    continue = true
    offset = 0

    while continue

    send_data = {
      :key => WsClient::API_KEY,
      :project_id => @finance_issue_project_id.to_s,
      :limit => LIMIT.to_s,
      :offset => offset.to_s,
      :tracker_id => @tracker.to_s
    }

    response_data = @ws_client.http_get("issues.xml", send_data)
    doc = REXML::Document.new(response_data)

    total_count =  doc.elements['issues'].attributes["total_count"].to_i
    limit = doc.elements['issues'].attributes["limit"].to_i
    offset = doc.elements['issues'].attributes["offset"].to_i

    doc.elements.each('issues/issue') do |element|
      hash = {
        :id => element.elements["id"].text,
        :tracker => element.elements["tracker"].attributes["name"],
        :status => element.elements["status"].attributes["name"],
        :start_date => element.elements["start_date"].text,
        :due_date => element.elements["due_date"].text
      }

      element.elements["custom_fields"].each do |subelement|
        custom_field_id = subelement.attributes["id"]
        hash[:sales] = subelement.elements["value"].text if custom_field_id == "86"
      end

      hash[:subject_origin] = element.elements["subject"].text
      get_issue_detail(hash[:id], hash)

      if @project_id == PROJECT_ID_LA
        weekly = weekly?(hash[:id])
        if weekly == @weekly
          if @weekly
            hash[:subject] = "#{element.elements["subject"].text} [#{@week_label}]"
            logging "[FinanceTicket] Weekly: #{hash[:subject]}"
          else
            hash[:subject] = "#{element.elements["subject"].text} [#{@month_label}]"
            logging "[FinanceTicket] Monthly: #{hash[:subject]}"
          end
          targets << FinanceIssue.new(hash)
        end
      else
        hash[:subject] = "#{element.elements["subject"].text} [#{@month_label}]"
        logging "[FinanceTicket] Monthly: #{hash[:subject]}"
        targets << FinanceIssue.new(hash)
      end

    end
      continue = false if total_count < limit + offset
      offset += limit
    end
    targets
  end

  # Retrun boolean by the inputed id
  def weekly?(id)
    send_data = {
        :key => WsClient::API_KEY
    }
    response_data = @ws_client.http_get("issues/#{id}.xml", send_data)
    doc = REXML::Document.new(response_data)

    doc.elements.each('*/custom_fields/custom_field') do |subelement|
      custom_field_id = subelement.attributes["id"]
      return true if custom_field_id == "215" and subelement.elements["value"].text == "1"
    end
    return false
  end

  # Get detail information for finance ticket
  def get_issue_detail(id, hash)
    send_data = {
        :key => WsClient::API_KEY
    }
    response_data = @ws_client.http_get("issues/#{id}.xml", send_data)
    doc = REXML::Document.new(response_data)

    doc.elements.each('*/custom_fields/custom_field') do |subelement|
      custom_field_id = subelement.attributes["id"]
      hash[:prepaiduntil] = subelement.elements["value"].text if custom_field_id == "149"
      if custom_field_id == "118"
        hash[:contractual_hours] = subelement.elements["value"].text != nil ? subelement.elements["value"].text.to_i : 0
      end
      if custom_field_id == "117"
        hash[:sow_type] = subelement.elements["value"].text != nil ? subelement.elements["value"].text : ''
      end
    end
  end

  # Collect all projects
  def grab_all_projects
    #http://localhost:3000/projects.xml?key=e383ffd1ed58d3e673fa6eac81034945fb2589bf

    continue = true
    offset = 0

    while continue
      send_data = {
        :key => WsClient::API_KEY,
        :limit => LIMIT.to_s,
        :offset => offset.to_s
      }

      response_data = @ws_client.http_get("projects.xml", send_data)
      doc = REXML::Document.new(response_data)

      total_count =  doc.elements['projects'].attributes["total_count"].to_i
      limit = doc.elements['projects'].attributes["limit"].to_i
      offset = doc.elements['projects'].attributes["offset"].to_i

      doc.elements.each('projects/project') do |element|
        @project_map[element.elements["name"].text.strip] = element.elements["identifier"].text
        logging "[Project] #{element.elements["name"].text.strip} / #{element.elements["identifier"].text}"
      end
      continue = false if total_count < limit + offset

      offset += limit
    end
  end

  # Collect all existing invoice tickets
  #http://192.168.56.101:3000//issues.xml?key=e383ffd1ed58d3e673fa6eac81034945fb2589bf&limit=100&offset=0&project_id=813
  def grab_all_invoicing_issues
    @invoicing_list = []
    [1, 66].each do |status|  #New, Paid
      continue = true
      offset = 0
      while continue
        send_data = {
          :key => WsClient::API_KEY,
          :project_id => @finance_issue_project_id.to_s,
          :limit => LIMIT.to_s,
          :offset => offset.to_s,
          :status_id => status.to_s,
          :tracker_id => "62" #Invoicing
        }

        response_data = @ws_client.http_get("issues.xml", send_data)
        doc = REXML::Document.new(response_data)

        total_count =  doc.elements['issues'].attributes["total_count"].to_i
        limit = doc.elements['issues'].attributes["limit"].to_i
        offset = doc.elements['issues'].attributes["offset"].to_i

        doc.elements.each('issues/issue') do |element|
          @invoicing_list << element.elements["subject"].text
          logging "[ExistingInvoice] #{element.elements["subject"].text}"
        end

        continue = false if total_count < limit + offset
        offset += limit
      end
    end
  end

  # Get last month's billable hour from Billing Plugin Web Service
  def get_last_billable_hour(idtr)
    send_data = {
      :key => WsClient::API_KEY
    }
    response_data = @ws_client.http_get("projects/#{idtr}/billing.json", send_data)

    last_billable_hour = 0.0
    begin
      result = JSON.parse(response_data)

      last_billable_hour = result["last_billable_hour"]
    rescue => e
      logging "JSON Perse Error: " + e.message
    end
    last_billable_hour
  end

  def executer
    grab_all_projects

    unless @delivery
      @tracker = 49 # T&M
      targets = grab_standing_finance_issues
      grab_all_invoicing_issues

      targets.each do |target|
        unless @invoicing_list.include? target.subject
          logging "Target: #{target.subject}"
          prepaiduntil = Date.strptime(target.prepaiduntil, "%Y-%m-%d") rescue Date.today
          if Date.today >= prepaiduntil
            priority="4" # 4: Normal
            status = "1" # 1: New

            last_billable_hour = get_last_billable_hour(@project_map[target.subject_origin])

            if last_billable_hour == 0 and target.contractual_hours > 0
              priority = "5" # 5: High
            elsif last_billable_hour == 0
              unless target.sow_type == "Maintenance"
                status = "66" # 66: Paid
              end
            end
            json = create_json(target, status, priority)
            create_invoice_ticket(target, json)
          else
            logging "Out of scope because of Pre Paid Until %s" % [target.prepaiduntil]
          end

        else
          logging "Already exist. #{target.subject}"
        end
      end
    else
      @tracker = 53 #delivery
      targets = grab_standing_finance_issues
      targets.each do |target|
        if target.due_date == Date.today.strftime("%Y-%m-%d")

          json = create_json(target)
          create_invoice_ticket(target, json)
        else
          logging "Out of scorp. Due date is #{target.due_date} : #{target.subject}"
        end
      end
    end

    SendMail::delivery([], [], "[Redmine] Invoice Generator", @log_buffer, [], @test_mode)
  end

  private
  def create_json(target, status="1", priority="4")
    data = <<-EOF
{
    "issue": {
      "project_id": "#{@finance_issue_project_id}",
      "subject": "#{target.subject}",
      "status_id": "#{status}",
      "tracker_id": "62",
      "priority_id": "#{priority}",
      "description": "This ticket was generated by Redmine Client tool automatically.",
      "custom_field_values": {
                                "86": "#{target.sales}"
                            }
             }
}
EOF
  end

  def create_invoice_ticket(target, json)
    logging "Create invoice. #{target.subject}"
    logging "---Input Start-----------------------------------------------------"
    logging json
    logging "---Input End  -----------------------------------------------------"
    response_data = @ws_client.http_post("issues.json", WsClient::API_KEY, json)
    logging "---Output Start-----------------------------------------------------"
    logging response_data
    logging "---Output End  -----------------------------------------------------"
  end

  def logging(message, *args)
    puts message % args
    @log_buffer << message % args + "\n"
  end
end


if __FILE__ == $0
  unless ARGV.size == 4
    puts USEAGE
    exit 99
  end
  location = ARGV[0]
  delivery = ARGV[1] == "delivery" ? true : false rescue false
  defualt_weekly = ARGV[2] == "yes" ? true : false rescue false
  defualt_host_is_test = ARGV[3] == "yes" ? true : false rescue true

  invoice_maker = InvoiceMaker.new(location, delivery, defualt_weekly, defualt_host_is_test)
  invoice_maker.executer

end




