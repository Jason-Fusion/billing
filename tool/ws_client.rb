require "net/http"
require "net/https"
require "uri"
require 'rexml/document'
require "date"

class WsClient
  API_KEY = "e383ffd1ed58d3e673fa6eac81034945fb2589bf"

  def initialize(test=true)
    unless test
      @host = "54.199.158.75"
      @port = 80
      @base_url = "http://#{@host}:#{@port}"
    else
      @host = "54.64.58.116"
      @port = 80
      @base_url = "http://#{@host}:#{@port}"
    end
    @test = test
  end

  def http_get(uri_dir, send_data)
    uri = URI.parse("#{@base_url}/#{uri_dir}")
    response = nil

    http = Net::HTTP.new(uri.host, uri.port)
    query_string = (send_data||{}).map{|k,v|
      URI.encode(k.to_s) + "=" + URI.encode(v.to_s)
    }.join("&")

    request = Net::HTTP::Get.new(uri.path + "?" + query_string)
    response = http.request(request)

    response.body rescue nil
  end

  def http_put(uri_dir, send_data, data)
    uri = URI.parse("#{@base_url}/#{uri_dir}")
    response = nil

    http = Net::HTTP.new(uri.host, uri.port)

    query_string = (send_data||{}).map{|k,v|
      URI.encode(k.to_s) + "=" + URI.encode(v.to_s)
    }.join("&")
    headers = {'Content-Type' => 'application/json; charset=utf-8'}

    response = http.send_request('PUT', uri.request_uri + "?" + query_string, data, headers)

    response.body rescue nil
  end

  def http_post(uri_dir, api_key, data)
    uri = URI.parse("#{@base_url}/#{uri_dir}")
    response = nil

    http = Net::HTTP.new(uri.host, uri.port)

    request = Net::HTTP::Post.new(uri.path)
    request.content_type = 'application/json'
    request['X-Redmine-API-Key'] = api_key
    request.body = data

    response = http.request(request)
    p response
    response.body rescue nil
  end
end

if __FILE__ == $0
  ws_client = WsClient.new false

    continue = true
    offset = 0

    while continue
      send_data = {
        :key => @api_key,
        :project_id => "813",
        :limit => "100",
        :offset => offset.to_s,
        :tracker_id => "62" #Invoicing
      }

      response_data = ws_client.http_get("issues.xml", send_data)
      doc = REXML::Document.new(response_data)
      total_count =  doc.elements['issues'].attributes["total_count"].to_i
      limit = doc.elements['issues'].attributes["limit"].to_i
      offset = doc.elements['issues'].attributes["offset"].to_i

      doc.elements.each('issues/issue') do |element|
        puts element
      end

      offset += limit
      continue = false if total_count < limit + offset
    end

end

