# Redmine Clinet Tool
# Copyright (C) 2014 Fusion Systems Japan
#
# This program is a part of plugin called Charts Plugin of Redmine.
# This program generate invoicing issues on Redmine automatically.
#
# Auther: Hidekazu Inoue
#
require "./ws_client"
require "net/https"
require "uri"
require 'rexml/document'
require "date"
require 'rubygems'
require 'active_support/all'
require 'json'
require './sendmail'
require 'csv'

USEAGE = <<-EOF
Useage
ruby report.rb <test mode>

  test mode
    Testing(localhost) -> yes
    Live -> no

e.g.)
ruby report.rb no

EOF

LABEL_TODAY = Date.today.strftime("%Y-%m-%d")
REPORT_PATH = "/tmp/report_#{LABEL_TODAY}.csv"

FINANCE_ISSUE_PROJECT_ID_TK = 813  # Tokyo Finace Project ID
FINANCE_ISSUE_PROJECT_ID_SH = 814  # Shnaghai Finace Project ID
FINANCE_ISSUE_PROJECT_ID_LA = 815  # LA Finace Project ID
FINANCE_ISSUE_PROJECT_ID_HK = 816  # HK Finace Project ID
PROJECT_ID_TK = "itms-financials"
PROJECT_ID_SH = "itms-financials-sh"
PROJECT_ID_LA = "itms-financials-la"
PROJECT_ID_HK = "itms"

LIMIT = 100 # Limmitaiton for the maximum data number of Redmine web service

#TO_ADDRESSES = ["hidekazu.inoue@fusionsystems.co.jp"]
TO_ADDRESSES = ["jason.zudell@fusionsystems.co.jp", "robert.corrigan@fusionsystems.co.jp"]

class FinanceIssue
  attr_accessor :id, :subject, :tracker, :status, :sales, :start_date, :due_date, \
                :subject_origin, :tracker_id
  def initialize(values)
    @contractual_hours = 0
    values.each do |k, v|
      self.send("#{k}=", v)
    end
  end
end

class ReportMaker

  include SendMail

  def initialize(location="tk", test_mode=true)
    case location
    when "tk"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_TK
      @project_id = PROJECT_ID_TK
    when "sh"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_SH
      @project_id = PROJECT_ID_SH
    when "la"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_LA
      @project_id = PROJECT_ID_LA
    when "hk"
      @finance_issue_project_id = FINANCE_ISSUE_PROJECT_ID_HK
      @project_id = PROJECT_ID_HK
    else
      raise "oops! confirm arguments #{location}"
    end

    @ws_client = WsClient.new test_mode
    @test_mode = test_mode
    @today = Date.today

    @project_map = {}
    @target_issues = []
    @log_buffer = ''
  end

  # Collect all projects
  def grab_all_projects
    #http://localhost:3000/projects.xml?key=e383ffd1ed58d3e673fa6eac81034945fb2589bf

    continue = true
    offset = 0

    while continue
      send_data = {
        :key => WsClient::API_KEY,
        :limit => LIMIT.to_s,
        :offset => offset.to_s
      }

      response_data = @ws_client.http_get("projects.xml", send_data)
      doc = REXML::Document.new(response_data)

      total_count =  doc.elements['projects'].attributes["total_count"].to_i
      limit = doc.elements['projects'].attributes["limit"].to_i
      offset = doc.elements['projects'].attributes["offset"].to_i

      doc.elements.each('projects/project') do |element|
        @project_map[element.elements["name"].text.strip] = element.elements["identifier"].text
        #logging "[Project] #{element.elements["name"].text.strip} / #{element.elements["identifier"].text}"
      end
      continue = false if total_count < limit + offset

      offset += limit
    end
  end

  # Collect all finance tickets
  def grab_standing_finance_issues
    [49, 53].each do |tracker|
      send_data = {
        :key => WsClient::API_KEY,
        :project_id => @finance_issue_project_id.to_s,
        :limit => LIMIT.to_s,
        :tracker_id => tracker.to_s
      }

      response_data = @ws_client.http_get("issues.xml", send_data)
      doc = REXML::Document.new(response_data)

      doc.elements.each('issues/issue') do |element|
        hash = {
          :id => element.elements["id"].text,
          :tracker => element.elements["tracker"].attributes["name"],
          :status => element.elements["status"].attributes["name"],
          :start_date => element.elements["start_date"].text,
          :due_date => element.elements["due_date"].text,
          :tracker_id => tracker
        }

        element.elements["custom_fields"].each do |subelement|
          custom_field_id = subelement.attributes["id"]
          hash[:sales] = subelement.elements["value"].text if custom_field_id == "86"
        end

        hash[:subject_origin] = element.elements["subject"].text.strip
        @target_issues << FinanceIssue.new(hash)
      end
    end
  end

  def executer
    grab_all_projects
    grab_standing_finance_issues

    rows = []
    rows << [
        "Project",
        "Tracker",
        "Beginning Date",
        "End Date",
        "Revenue"
      ]

    @target_issues.each do |issue|
      logging "Issue Title: %s" % [issue.subject_origin]

      idtr = @project_map[issue.subject_origin]
      unless idtr == nil
        rows << get_billing(issue.subject_origin, idtr)
      else
        logging "Not found #{issue.subject_origin} in Project List. Please check SOW ticket naming.\n"
      end
      #logging ""
    end

    CSV.open(REPORT_PATH, "w", {:force_quotes => true}) do |csv|
      rows.each do |row|
      csv << row
#        unless "fixed" == row[1]
#        csv << row
#        else
#          due_date = Date.strptime(row[3], "%Y-%m-%d")
#          if @today.months_ago(1).change(:day => 1) == due_date.change(:day => 1)
#            csv << row
#          end
#        end
      end
    end
    SendMail::delivery(TO_ADDRESSES, [], "[Redmine - AWS] Last Month Revenue Report", @log_buffer, [REPORT_PATH], @test_mode)
  end

  private

  def get_billing(project_name, idtr)
    send_data = {
      :key => WsClient::API_KEY
    }
    response_data = @ws_client.http_get("projects/#{idtr}/billing.json", send_data)

    begin
      result = JSON.parse(response_data)

      revenue = result["revenue"]
      return [
        project_name,
        result["tracker"],
        result["start_date"],
        result["end_date"],
        result["revenue"]
      ]
    rescue => e
      logging "JSON Perse Error: " + e.message
      logging "Confirm Billing Plugin Setting or SOW Ticket Setting\n"
      return [
        project_name,
        "Parse Error",
        "N/A",
        "N/A",
        "0.0"
      ]
    end
  end

  def logging(message, *args)
    puts message % args
    @log_buffer << message % args + "\n"
  end
end

if __FILE__ == $0
  unless ARGV.size == 1
    puts USEAGE
    exit 99
  end
  host_is_test = ARGV[0] == "yes" ? true : false rescue true
  report_maker = ReportMaker.new('tk', host_is_test)
  report_maker.executer

end




