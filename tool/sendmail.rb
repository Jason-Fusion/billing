require 'rubygems'
#require '/var/lib/gems/1.8/gems/mail-2.5.3/lib/mail.rb'


BCC_ADDRESSES = ["jason.zudell@fusionsystems.co.jp"]

module SendMail
  def self.delivery(receiver, receiver_cc, subject="test", body="body", attachments=[], test_mode=true)
    if test_mode
      require "mail"
    else
      require "mail"
    end
    
    options = {
    :address              => "tokyo.fusionsystems.org",
    :port                 => 25,
    :domain               => "tokyo.fusionsystems.org",
    :user_name            => "helpdesk@fusionsystems.org",
    :password             => "Password@",
    :authentication       => :login
    }
    
    Mail.defaults do
      delivery_method :smtp, options
    end

    mail = Mail.new do
      to receiver.join(", ")
      cc receiver_cc.join(", ")
      bcc BCC_ADDRESSES.join(", ")
      from "helpdesk@fusionsystems.org"
    end
    mail.charset ='utf-8'
    mail.subject = subject
    mail.body  = body
    attachments.each do |attachment|
      mail.add_file(attachment)
    end

    mail.deliver unless test_mode
  end

end


if __FILE__ == $0
  SendMail::delivery([], [], "test-title", "test-body", false)
end

