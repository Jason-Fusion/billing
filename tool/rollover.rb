require "./ws_client"
require "date"
require 'rubygems'
require 'active_support/all'

CONTRACT_TYPE_SUPPORT = 49


FINANCE_ISSUE_PROJECT_ID_TK = 813  # Tokyo Finace Project ID
FINANCE_ISSUE_PROJECT_ID_SH = 814  # Shnaghai Finace Project ID
FINANCE_ISSUE_PROJECT_ID_LA = 815  # LA Finace Project ID
FINANCE_ISSUE_PROJECT_ID_HK = 816  # HK Finace Project ID
PROJECT_ID_TK = "itms-financials"
PROJECT_ID_SH = "itms-financials-sh"
PROJECT_ID_LA = "itms-financials-la"
PROJECT_ID_HK = "itms"

LIMIT = 100

class Rollover

  def initialize(test)
    @ws_client = WsClient.new test
    @sows = []
    @base_date = Date.today
    @project_map = {}
    #Date.strptime(params[:base_date], "%Y-%m-%d") rescue nil
  end

  def grab_tickets
    continue = true
    offset = 0

    while continue
      #http://localhost:3000/issue.xml?key=e383ffd1ed58d3e673fa6eac81034945fb2589bf&project_id=813&tracker_id=49
      send_data = {
        :key => WsClient::API_KEY,
        :project_id => FINANCE_ISSUE_PROJECT_ID_TK.to_s,
        :limit => LIMIT.to_s,
        :offset => offset.to_s,
        :tracker_id => CONTRACT_TYPE_SUPPORT.to_s
      }

      response_data = @ws_client.http_get("issues.xml", send_data)
      doc = REXML::Document.new(response_data)

      total_count =  doc.elements['issues'].attributes["total_count"].to_i
      limit = doc.elements['issues'].attributes["limit"].to_i
      offset = doc.elements['issues'].attributes["offset"].to_i

      doc.elements.each('issues/issue') do |element|
        hash = {
          :id => element.elements["id"].text,
          :subject => "#{element.elements["subject"].text.strip}",
          :tracker => element.elements["tracker"].attributes["name"],
          :status => element.elements["status"].attributes["name"],
          :start_date => element.elements["start_date"].text,
          :due_date => element.elements["due_date"].text
        }

        hash[:custom_fields] = {}

        element.elements["custom_fields"].each do |subelement|
          custom_field_id = subelement.attributes["id"]
          hash[:custom_fields][custom_field_id] = subelement.elements["value"].text
        end

        unless hash[:custom_fields]["229"] == "1"
          @sows << hash
        else
          puts "#{hash[:subject]} is out of target because of ingore PO tag"
        end
      end

      offset += limit
      continue = false if total_count < limit + offset
    end
  end

  def grab_all_projects
    #uri = URI.parse("#{@uri_base}/projects.xml")
    #http://localhost:3000/projects.xml?key=e383ffd1ed58d3e673fa6eac81034945fb2589bf

    continue = true
    offset = 0

    while continue
      send_data = {
        :key => WsClient::API_KEY,
        :limit => LIMIT.to_s,
        :offset => offset.to_s
      }

      response_data = @ws_client.http_get("projects.xml", send_data)
      #response_data = http_get(uri, send_data)
      doc = REXML::Document.new(response_data)

      total_count = doc.elements['projects'].attributes["total_count"].to_i
      limit = doc.elements['projects'].attributes["limit"].to_i
      offset = doc.elements['projects'].attributes["offset"].to_i

      doc.elements.each('projects/project') do |element|
        #p element
        @project_map[element.elements["name"].text.strip] = element.elements["identifier"].text

        puts "[Project] #{element.elements["name"].text.strip} / #{element.elements["identifier"].text}"
      end
      continue = false if total_count < limit + offset

      offset += limit
    end


    p @project_map.size
  end

  def get_next_rollover(idtr)
    send_data = {
      :key => WsClient::API_KEY
    }
    response_data = @ws_client.http_get("projects/#{idtr}/billing.json", send_data)

    #p "projects/#{idtr}/billing.json"
    #p response_data

    last_rollover_hours = 0.0
    begin
      require 'json'
      result = JSON.parse(response_data)

      last_rollover_hours = result["last_rollover_hour"]
      #response_data

      puts "last rollover hours: #{last_rollover_hours}"
    rescue => e
      puts "JSON Perse Error: " + e.message
    end
    last_rollover_hours
  end

  def executer
    #http://localhost:3000/projects.xml?key=e383ffd1ed58d3e673fa6eac81034945fb2589bf
    @sows.each do |sow|
      puts "----------------------------------------------------------------------------"
      puts "Contract Type: " + sow[:custom_fields]["117"]
      if sow[:custom_fields]["117"] == "T&M"
        send_data = {
          :key => WsClient::API_KEY
        }

        puts "Project: " + sow[:subject]
        puts "Last Month Date: " + sow[:custom_fields]["209"].to_s # Last month rollover date
        puts "Last Month Hour: " + sow[:custom_fields]["239"].to_s # Last month rollover date
        puts "This Month Date: " + sow[:custom_fields]["210"].to_s # This month rollover date
        puts "This Month Hour: " + sow[:custom_fields]["238"].to_s # This month rollover date

        rollover = 0.0

        unless @project_map[sow[:subject]] == nil
          rollover = get_next_rollover @project_map[sow[:subject]]

          puts "Rollover: %5.2f hours" % [rollover]
          #unless rollover == 0.0

          #p sow[:custom_fields]["210"].class.to_s
          last_month_date = Date.strptime(sow[:custom_fields]["209"].to_s, "%Y-%m-%d") rescue nil
          this_month_date = Date.strptime(sow[:custom_fields]["210"].to_s, "%Y-%m-%d") rescue nil

          # Last Month Update
          if this_month_date != nil and this_month_date.change(:day => 1) == @base_date.change(:day => 1) << 1
            data = create_lastmonth_json(sow[:custom_fields]["238"].to_s, sow[:custom_fields]["210"])
            response_data = @ws_client.http_put("issues/#{sow[:id]}.xml", send_data, data)
            if response_data != nil
              puts "Update Last Month %s: %s hours" % [sow[:custom_fields]["238"].to_s, sow[:custom_fields]["210"]]
            else
              puts "error on update last month"
            end
            #p response_data
          else
            puts "No need to move last month data"
          end

          # This Month Update
          if this_month_date == nil or this_month_date.change(:day => 1) == @base_date.change(:day => 1) << 1 or this_month_date.change(:day => 1) == @base_date.change(:day => 1)
            data = create_thismonth_json(rollover.to_s, @base_date.strftime("%Y-%m-%d"))
            response_data = @ws_client.http_put("issues/#{sow[:id]}.xml", send_data, data)
            if response_data != nil
              puts "Update This Month %s: %s hours" % [rollover.to_s, @base_date.strftime("%Y-%m-%d")]
            else
              puts "error on update this month"
            end
          else
            data = create_thismonth_json(rollover.to_s, @base_date.strftime("%Y-%m-%d"))
            response_data = @ws_client.http_put("issues/#{sow[:id]}.xml", send_data, data)
            if response_data != nil
              puts "Update This Month %s: %s hours" % [rollover.to_s, @base_date.strftime("%Y-%m-%d")]
            else
              puts "error on update this month"
            end
          end

        end
      else
        puts "---> Non T&M Skip!!"
      end
    end
  end

=begin
  def update_rollover
      send_data = {
        :key => WsClient::API_KEY
      }
    data = create_json()
    response_data = @ws_client.http_put("issues/9865.xml", send_data, data)
    p response_data
  end
=end
  def create_lastmonth_json(hours, date)
    data = <<-EOF
{
    "issue": {
      "custom_field_values": {
                              "239": "#{hours}",
                              "209": "#{date}"
                            }
    }
}
EOF
  end


  def create_thismonth_json(hours, date)
    data = <<-EOF
{
    "issue": {
      "custom_field_values": {
                              "238": "#{hours}",
                              "210": "#{date}"
                            }
    }
}
EOF
  end

  def create_json(last_hours, this_hours, last_date, this_date)
    data = <<-EOF
{
    "issue": {
      "custom_field_values": {
                              "239": "#{last_hours}",
                              "238": "#{this_hours}",
                              "209": "#{last_date}",
                              "210": "#{this_date}"
                            }
    }
}
EOF
  end

end

if __FILE__ == $PROGRAM_NAME
  test_mode = false
  rollover = Rollover.new test_mode

  #rollover.grab_tickets
  #rollover.executer

  rollover.grab_tickets
  rollover.grab_all_projects
  rollover.executer
  #rollover.get_next_rollover
end


