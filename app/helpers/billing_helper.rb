module BillingHelper
  def summary_table_maintenance(project, collection, context)
    buff = ""
    buff << "<table border=\"0\" >"
    buff << "  <tr>"
    buff << "    <td>Contract Name </td><td>%s</td>"%[project.name]
    buff << "  </tr><tr>"
    buff << "    <td>Beginning Date </td><td>%s</td>"%[collection.beginning_date]
    buff << "  </tr><tr>"
    buff << "    <td>End Date </td><td>%s</td>"%[collection.end_date]
    buff << "  </tr><tr>"
    buff << "    <td width=\"250\" >Total Hours</td><td width=\"630\">%s Hours</td>"%
                     [collection.total_hours]

    buff << "  </tr><tr>"
    buff << "    <td>Charge </td><td>%s</td>"%[collection.final_charge]
    buff << "  </tr>"
    buff << "</table>"
    buff.html_safe
  end

  def summary_table_miscellaneous(project, collection, context)
    buff = ""
    buff << "<table border=\"0\" >"
    buff << "  <tr>"
    buff << "    <td>Contract Name </td><td>%s</td>"%[project.name]
    buff << "  </tr><tr>"
    buff << "    <td>Beginning Date </td><td>%s</td>"%[collection.beginning_date]
    buff << "  </tr><tr>"
    buff << "    <td>End Date </td><td>%s</td>"%[collection.end_date]
    buff << "  </tr><tr>"
    buff << "    <td width=\"250\" >Total Hours</td><td width=\"630\">%s Hours</td>"%
                     [collection.total_hours]

    buff << "  </tr><tr>"
    buff << "    <td>Accumulated Charges </td><td>%s</td>"%[collection.final_charge]
    buff << "  </tr>"
    buff << "</table>"
    buff.html_safe
  end
  
  def summary_table_fixed(project, billing, context)
    buff = ""
    buff << "<table border=\"0\" >"
    buff << "  <tr>"
    buff << "    <td>Contract Name </td><td>%s</td>"%[project.name]
    buff << "  </tr><tr>"
    buff << "    <td>Beginning Date </td><td>%s</td>"%[project.project_start_date]
    buff << "  </tr><tr>"
    buff << "    <td>End Date </td><td>%s</td>"%[project.project_due_date]
    buff << "  </tr><tr>"
    buff << "    <td width=\"250\" >Total Hours</td><td width=\"630\">%s Hours</td>"%
                     [billing.project_total_hours]

    buff << "  </tr><tr>"
    buff << "    <td>Charge</td><td>%s</td>"%  
                     [project.value_to_fusion]
    buff << "  </tr>"
    buff << "</table>"
    buff.html_safe
  end
  
  
  def summary_table(project, collection, context)
    buff = ""
    buff << "<table border=\"0\" >"
    buff << "  <tr>"
    buff << "    <td>Contract Name </td><td>%s</td>"%[project.name]
    buff << "  </tr><tr>"
    buff << "    <td>Beginning Date </td><td>%s</td>"%[collection.beginning_date]
    buff << "  </tr><tr>"
    buff << "    <td>End Date </td><td>%s</td>"%[collection.end_date]
    buff << "  </tr><tr>"
    buff << "    <td width=\"250\" >Total Hours</td><td width=\"630\">%s Hours</td>"%
                     [collection.total_hours]

    buff << "  </tr><tr>"
    buff << "    <td>Billable Hours </td><td>%s Hours</td>"%  
                     [collection.billable_total_hours]
    buff << "  </tr><tr>"
    buff << "    <td> - Standard Hours </td><td>%s Hours</td>"%  
                     [collection.standard_hours]
    buff << "  </tr><tr>"
    buff << "    <td> - Non-Standard Hours </td><td>%s Hours</td>"%  
                     [collection.nonstandard_hours]
    
    unless context.client_restrict
      buff << "  </tr><tr>"
      buff << "    <td>Non-Billable Hours </td><td>%s Hours</td>"%  
                     [collection.nonbillable_total_hours]
    end
    
    unless project.tm_simple? or project.miscellaneous?
    buff << "  </tr><tr>"
    buff << "    <td>Contractual Hours </td><td>%s Hours</td>"%
                     [project.contractual_hours]
    buff << "  </tr><tr>"
    buff << "    <td>Rollover Hours </td><td>%s Hours</td>"%
                     [collection.rollover_hours]
    buff << "  </tr><tr>"
    buff << "    <td>Contractual and Rollover Hours </td><td>%s Hours</td>"%
                     [collection.contractual_and_rollover_hours]
    
    buff << "  </tr><tr>"
    buff << "    <td>Remaining Hours </td><td>%s Hours</td>"%  
                     [collection.remaining_hours]
    end
    
    unless context.client_restrict
      buff << "  </tr><tr>"
      buff << "    <td>Billable Ratio </td><td>%s %s</td>"% 
                     [collection.billable_ratio, '%']
      buff << "  </tr><tr>"
      buff << "    <td>Non Billable Ratio </td><td>%s %s</td>"%  
                     [collection.nonbillable_ratio, '%']
    end
    
    unless context.client_restrict or project.tm_simple? or project.miscellaneous?
      buff << "  </tr><tr>"
      buff << "    <td>Cotractual Minimum Charge </td><td>%s</td>"%  
                     [collection.minimum_charge]
    end
    
    buff << "  </tr><tr>"
    buff << "    <td>Charge</td><td>%s</td>"%  
                     [to_number_with_delimiter(collection.final_charge, true)]
    
    unless context.client_restrict or project.tm_simple? or project.miscellaneous?
      message = collection.description.join("<br/>")
      buff << "  </tr><tr>"
      buff << "    <td>Details</td><td>%s</td>"%  
                     [message]
    end
    
    buff << "  </tr>"
    buff << "</table>"
    buff.html_safe
  end
  
  def tm_contract_header(project, collection)
    buff = ""
    buff << "<table border=\"0\">"
    buff << "  <tr>"
    buff << "    <td>Contract Name </td><td>%s</td>" % [project.name]
    buff << "  </tr><tr>"
    buff << "    <td>Beginning Date </td><td>%s</td>" % [collection.beginning_date]
    buff << "  </tr><tr>"
    buff << "    <td>End Date </td><td>%s</td>" % [collection.end_date]

    buff << "  </tr><tr>"
    buff << "    <td>Contractual Hours </td><td>%f</td>" % [project.contractual_hours]
    buff << "  </tr><tr>"
    buff << "    <td>Fixed Contractual Hourly Rate </td><td>%f</td>" %
      [project.fixed_contractual_hourly_rate]
    buff << "  </tr><tr>"
    buff << "    <td>Location </td><td>%s</td>" % [project.location]
    
    collection.billable_stock.each_key do |key|
      buff << "  </tr><tr>"
      buff << "    <td>Rate %s </td><td>%s</td>" % [key, project.custom_value[key]]
    end
    
    buff << "  </tr><tr>"
    buff << "  </tr>"
    buff << "</table>"
    buff.html_safe
  end
  
  
  def transaction_table(collection, billable_option=true)
    buff = ""
    counter = 0
    
    coler_changer = true
    buff << "<table width=\"878\" border=\"0\" cellspacing=\"1\" bgcolor=\"black\">"
    buff << "<tr>"
    buff << "<th bgcolor=\"gray\" width=\"80\" ><font color=\"#ffffff\">Date</font></td>"
    buff << "<th bgcolor=\"gray\" width=\"120\"><font color=\"#ffffff\">In Charge</font></th>"
    buff << "<th bgcolor=\"gray\" width=\"100\"><font color=\"#ffffff\">Tracker</font></th>"
    buff << "<th bgcolor=\"gray\" width=\"150\"><font color=\"#ffffff\">Activity</font></th>"
    buff << "<th bgcolor=\"gray\"              ><font color=\"#ffffff\">Comments</font></th>"
    buff << "<th bgcolor=\"gray\" width=\"30\"><font color=\"#ffffff\">Hours</font></th>"
    buff << "</tr>"
    
    collection.sort.each do |time_entry|
    
      if time_entry.billable? == billable_option
        counter += 1
        if coler_changer
          buff << '<tr bgcolor="#F0F0F0">' 
          coler_changer = false
        else
          buff << '<tr bgcolor="white">' 
          coler_changer = true
        end
        link = "<a href=\"../../time_entries/#{time_entry.id.to_s}/edit\" data-method=\"get\">#{time_entry.hours.to_s}</a>"
          
        buff << "<td align=\"center\" >%s</td>"%[time_entry.spent_on.strftime("%d %b %Y")]
        buff << "<td  align=\"center\" >%s</td>"%[time_entry.user_name.gsub(".", " ").titleize]
        buff << "<td  align=\"center\" >%s</td>"%[time_entry.tracker]
        buff << "<td >%s</td>"%[time_entry.activity]
        buff << "<td >%s</td>"%[time_entry.comments]
        buff << "<td align=\"right\">%s</td>"%[link]
        buff << "</tr>"
      end
    end  
    buff << "</table>"
    return counter > 0 ? buff.html_safe : "No Activity"
  end
  
  def to_number_with_delimiter(number, roundup=false)
    return number if number.kind_of? String
    number = number.round if roundup
    minus = false
    if number < 0
      number = number * -1
      minus = true
    end
    first, last = number.to_s.split(".")
    str_number = ""
    if last != nil
      str_number = first.reverse.gsub(/(\d{3})(?=\d)/, '\1,').reverse + "." + last
    else
      str_number = first.reverse.gsub(/(\d{3})(?=\d)/, '\1,').reverse
    end
    str_number = "-#{str_number}" if minus
    str_number
  end
end

