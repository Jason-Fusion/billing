module InvoiceHelper

  def transaction_table(collection, billable_option=true)
    buff = ""
    counter = 0
    
    coler_changer = true
    buff << "<table width=\"878\" border=\"0\" cellspacing=\"1\" bgcolor=\"black\">"
    buff << "<tr>"
    buff << "<th bgcolor=\"gray\" width=\"80\" ><font color=\"#ffffff\">Date</font></td>"
    buff << "<th bgcolor=\"gray\" width=\"120\"><font color=\"#ffffff\">In Charge</font></th>"
    buff << "<th bgcolor=\"gray\" width=\"100\"><font color=\"#ffffff\">Tracker</font></th>"
    buff << "<th bgcolor=\"gray\" width=\"150\"><font color=\"#ffffff\">Activity</font></th>"
    buff << "<th bgcolor=\"gray\"><font color=\"#ffffff\">Comments</font></th>"
    buff << "<th bgcolor=\"gray\" width=\"30\"><font color=\"#ffffff\">Hours</font></th>"
    buff << "</tr>"
    
    collection.sort.each do |time_entry|
      if time_entry.billable? == billable_option
        counter += 1
        if coler_changer
          buff << '<tr bgcolor="#F0F0F0">' 
          coler_changer = false
        else
          buff << '<tr bgcolor="white">' 
          coler_changer = true
        end
        link = "<a href=\"../../time_entries/#{time_entry.id.to_s}/edit\" data-method=\"get\">#{time_entry.hours.to_s}</a>"
          
        buff << "<td align=\"center\" >%s</td>"%[time_entry.spent_on.strftime("%d %b %Y")]
        buff << "<td  align=\"center\" >%s</td>"%[time_entry.user_name.gsub(".", " ").titleize]
        buff << "<td  align=\"center\" >%s</td>"%[time_entry.tracker]
        buff << "<td >%s</td>"%[time_entry.activity]
        buff << "<td >%s</td>"%[time_entry.comments]
        buff << "<td align=\"right\">%s</td>"%[link]
        buff << "</tr>"
      end
    end  
    buff << "</table>"
    return counter > 0 ? buff.html_safe : "No Activity"
  end

  def to_number_with_delimiter(number, roundup=false)
    return number if number.kind_of? String
    number = number.round if roundup
    minus = false
    if number < 0
      number = number * -1
      minus = true
    end
    first, last = number.to_s.split(".")
    str_number = ""
    if last != nil
      str_number = first.reverse.gsub(/(\d{3})(?=\d)/, '\1,').reverse + "." + last
    else
      str_number = first.reverse.gsub(/(\d{3})(?=\d)/, '\1,').reverse
    end
    str_number = "-#{str_number}" if minus
    str_number
  end
end

