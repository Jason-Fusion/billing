class BillingController < ApplicationController
  unloadable
  before_filter :find_project, :authorize, :only => :index

  accept_api_auth :index

  GROUP_NAMES = ["CAN_SEE_PROFITS_CHARTS", "CAN_SEE_BILLING_CHARTS"]

  def index

    if params[:base_date] != nil
      base_date = Date.strptime(params[:base_date], "%Y-%m-%d") rescue nil
      @context = Context.new(base_date, true, params[:debug].present?)
    else
      @context = Context.new(Date.today, false, params[:debug].present?)
    end

    if params[:print] != nil
      @context.print = true if params[:print] == "true"
    end

    begin
      authorization_check

      @data = DataAccessor.new(params[:project_id], @context)

      move_foward = @data.setup

      # In case of not bottom layer project
      return unless move_foward

      @data.grab_data

      if @data.fusion_project.tm?
        if @data.fusion_project.location == FsgProject::LOCATION_LA and @data.fusion_project.weekly_invoiced?
          @billing = WeeklyTmContractHoursSowCalculator.new(@data, @context)
        else
          @billing = MonthlyTmContractHoursSowCalculator.new(@data, @context)
        end
      elsif @data.fusion_project.tm_simple?
        if @data.fusion_project.location == FsgProject::LOCATION_LA and @data.fusion_project.weekly_invoiced?
          @billing = WeeklyTmSimpleSowCalculator.new(@data, @context)
        elsif @data.fusion_project.fusion_system_group
          @billing = MonthlyTmFsgSowCalculator.new(@data, @context)
        else
          @billing = MonthlyTmSimpleSowCalculator.new(@data, @context)
        end
      elsif @data.fusion_project.maintenance? and @data.fusion_project.location == FsgProject::LOCATION_TK
        @billing = MaintenanceSowCalculator.new(@data, @context)
      elsif @data.fusion_project.fixed? and @data.fusion_project.location == FsgProject::LOCATION_TK
        @billing = FixedSowCalculator.new(@data, @context)
      elsif @data.fusion_project.miscellaneous?
        @billing = MiscellaneousSowCalculator.new(@data, @context)
      else
        raise "No implementaion for %s and %s." % \
              [@data.fusion_project.type, @data.fusion_project.location]
      end

      @billing.setup
      @billing.calc

    rescue => e
      @context.set_error_happen
      @error = e.message
      @error += "\n" + e.backtrace.join("\n") if @context.debug_mode?
      return 0
    end



    respond_to do |format|
      format.html # index.html.erb
      format.csv  { send_data(report_to_csv(), :type => 'text/csv;', :filename => "billing_#{@data.fusion_project.identifier}.csv") }
#      format.pdf { send_data(:pdf => 'test', :type => 'application/pdf;', :encoding => 'UTF-8', :layout => 'invoice.html') }
#       format.pdf { send_data(:pdf => 'invoice', :type => 'application/pdf;', :orientation => 'Landscape', :encoding => 'UTF-8') }
      format.pdf do
        render :pdf => "invoice",
#               :orientation => 'Landscape',
               :template => 'invoice/_invoice.html.erb',
               :layout => 'invoice',
               :encoding    => 'UTF-8',
               :show_as_html => params[:debug].present?
      end

      format.json {
        # These calculations for monthly report only
        revenue = 0.0
        case @data.fusion_project.original_sow_type
        when "T&M" then
          if @data.fusion_project.custom_value["Pre-paid Until"] == nil
            revenue = @billing.get_last_collection.final_charge
          else
            revenue = @data.fusion_project.fixed_contractual_hourly_rate * @data.fusion_project.contractual_hours
          end
        when "Fixed" then
          revenue = @data.fusion_project.value_to_fusion / @data.fusion_project.get_term

        when "Client Site" then
          revenue = @data.fusion_project.value_to_fusion
        when "Manual" then
          revenue = @data.fusion_project.fixed_contractual_hourly_rate * @data.fusion_project.contractual_hours
        when "Maintenance" then
          revenue = @data.fusion_project.additional_service_fees
        end
        unless @data.fusion_project.fixed?

          render :json => {
            "project" => @data.fusion_project.name,
            "tracker" => @data.fusion_project.type,
            "last_rollover_hour" => @billing.get_last_collection.next_rollover_hours,
            "last_billable_hour" => @billing.get_last_collection.billable_total_hours,
            "start_date" => @billing.get_last_collection.beginning_date,
            "end_date" => @billing.get_last_collection.end_date,
            "revenue" => @billing.get_last_collection.final_charge
          }
        else
          render :json => {
            "project" => @data.fusion_project.name,
            "tracker" => @data.fusion_project.type,
            "start_date" => @data.fusion_project.project_start_date,
            "end_date" => @data.fusion_project.project_due_date,
            "revenue" => @data.fusion_project.value_to_fusion
          }

      end
      }
    end
  end

  private
  def find_project
    # @project variable must be set before calling the authorize filter
    @project = Project.find(params[:project_id])
  end

  def report_to_csv
    require 'fastercsv'
    csv_text = @billing.generate_csv() rescue FasterCSV.generate
    csv_text.to_s
  end

  def authorization_check(ok_group=GROUP_NAMES)
    flag = false
    User.current.groups.each do |group|
      if ok_group.include? group.name
        if group.name == "CAN_SEE_BILLING_CHARTS"
          @context.client_restrict = true
        end
        flag = true
      end
    end
    raise "You don't have a permission to access this page. " unless flag
  end
end

# gedit billing/app/controllers/billing_controller.rb billing/app/views/billing/* billing/lib/* &

