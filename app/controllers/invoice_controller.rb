class InvoiceController < BillingController
  unloadable
  before_filter :find_project, :authorize, :only => :index
  
  GROUP_NAMES = ["CAN_SEE_PROFITS_CHARTS", "CAN_SEE_INVOICE_CHARTS"]
  
  def index
    super
    
  end
    
  private  
  def find_project
    # @project variable must be set before calling the authorize filter
    @project = Project.find(params[:project_id])
  end
  
  def authorization_check
    super GROUP_NAMES
  end
end

