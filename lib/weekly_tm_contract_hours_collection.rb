class WeeklyTmContractHoursCollection < AbstractTmContractHoursCollection
  #attr_reader :beginning_date, :end_date
  def initialize(fusion_project, base_week)
    super(fusion_project)
    # Override values set by super class
    @beginning_date = base_week
    @end_date = base_week + 7
  end

  def issue_date
    @beginning_date + 2
  end

  def provisional_version?(date)
    return false
  end
end


