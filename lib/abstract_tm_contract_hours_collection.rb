
class AbstractTmContractHoursCollection < AbstractTmCollection
  STANDARD_BUSINESS_HOURS_LIST = [ "Weekday: 08:00-20:00", "Weekday: 08:00-18:00", "Weekday: 09:00-18:00" ]

  
  attr_reader :minimum_charge, :rollover_hours, :next_rollover_hours, \
              :description, :billable_stock
  
  
  def initialize(fusion_project)
    super(fusion_project)
    @minimum_charge = @fusion_project.contractual_hours *  @fusion_project.fixed_contractual_hourly_rate
    
    @next_rollover_hours = 0.0
  end
  
  def revenue
    return @revenue_sum > @minimum_charge ? @revenue_sum : @minimum_charge
  end # end method
  
  def actual_revenue
    return @revenue_sum
  end # end method
  
  def remaining_hours
    remaining_hours = @fusion_project.contractual_hours - @billable_total_hours
    return remaining_hours > 0.0 ? remaining_hours : 0.0
  end # end method  
  
  def contractual_and_rollover_hours
    return (@fusion_project.contractual_hours + @rollover_hours).truncate
  end
  
  def extra_hours
    extra_hours = @billable_total_hours - contractual_and_rollover_hours
    return extra_hours > 0.0 ? extra_hours : 0.0
  end
  
  def finalilze(rollover_hours=0.0)
    super()
    
    @rollover_hours = rollover_hours
    @final_charge = charge_to_client
  end
  
  def has_billable?
    return @billable_total_hours > 0.0 ? true : false
  end
  
  def provisional_version?
    raise "Need to implement this method on sub class"
  end
    
  private
  def charge_to_client
    extra_charge = 0.0
    extra_standard_hours = 0.0
    one_of_standard_rate = 0.0
    final_charge = 0.0
    
    @billable_detail_stock.each do |key, time_entries|
      if STANDARD_BUSINESS_HOURS_LIST.include? key
        
        time_entries.each do |time_entry|
          
          @standard_hours += time_entry.hours
          one_of_standard_rate = time_entry.client_rate if one_of_standard_rate == 0.0
          
#          if @standard_hours > @fusion_project.contractual_hours
#            extra_standard_hours += time_entry.hours
#            extra_standard_rate = time_entry.client_rate if extra_standard_rate == 0.0
#            extra_charge += time_entry.hours * extra_standard_rate
#          end
        end
      else
        time_entries.each do |time_entry|
          @nonstandard_hours += time_entry.hours
          extra_charge += time_entry.revenue
          @charge.add(time_entry.activity, time_entry.hours, time_entry.client_rate, :nonstandard)
        end
      end
      

    end
    
    if @standard_hours > @fusion_project.contractual_hours
      extra_standard_hours = @standard_hours - @fusion_project.contractual_hours
      
      extra_charge += one_of_standard_rate * extra_standard_hours
      @charge.add("Over Contract Hours", extra_standard_hours, one_of_standard_rate, :extrastandard)
    end

    #if @standard_hours > @fusion_project.contractual_hours
#    if extra_standard_hours > 0.0
      #extra_charge += @fusion_project.fixed_contractual_hourly_rate * (@standard_hours - @fusion_project.contractual_hours)
#      @charge.add("Over Contract Hours", extra_standard_hours, extra_standard_rate, :extrastandard)
#    end
    
    @description = []
    
    if @billable_total_hours <= self.contractual_and_rollover_hours
      @description << "Total Actual Hours (%f) <= [Contractual Hours (%f) + Rollover Hours (%f)]"%
        [@billable_total_hours, @fusion_project.contractual_hours, @rollover_hours]
      
      if @billable_total_hours == @standard_hours
        @description << "All booked hours are Standard Business Hours (%f)"% 
          [@standard_hours]
        @next_rollover_hours = (@fusion_project.contractual_hours - @billable_total_hours).to_f
        @description << "Next Rollover Hours (%f) = [Contractual Hours (%f)] - [Total Actual Hours (%f)]"%
          [@next_rollover_hours, @fusion_project.contractual_hours, @billable_total_hours]
        @description << "Create the Bill with one line [Contractual Hours (%f)] * [Fixed Contractual Hourly Rate (%f)]"%
          [@fusion_project.contractual_hours, @fusion_project.fixed_contractual_hourly_rate]
        final_charge = method_a
      else
        @description << "All booked hours are not Standard Business Hours. Starndard Business Hours (%f) Non Starndard Business Hours (%f)"% 
          [@standard_hours, @nonstandard_hours]
        @next_rollover_hours = (@fusion_project.contractual_hours - @standard_hours).to_f
        @description << "Next Rollover Hours (%f) = [Contractual Hours (%f)] - [the sum of hours booked against Standard Business Hours (%f)]"%
          [@next_rollover_hours, @fusion_project.contractual_hours, @standard_hours]
        final_charge = method_b(extra_charge)
        @description << "Create the Bill with one line [Contractual Hours (%f)] * [Fixed Contractual Hourly Rate (%f)] PLUS n instances of, [Hourly Rate] * hours, where n is number of cases of ![Standard Business Hours] booked. Extra charge (%f)."%
          [@fusion_project.contractual_hours, @fusion_project.fixed_contractual_hourly_rate, extra_charge]
      end
    else
      @description << "Total Actual Hours (%f) > [Contractual Hours (%f) + Rollover Hours (%f)]  "%
        [@billable_total_hours, @fusion_project.contractual_hours, @rollover_hours]
      
      if @standard_hours > @fusion_project.contractual_hours
        @description << "[the sum of hours booked against Standard Business Hours (%f)] > [Contractual Hours (%f)]" %
          [@standard_hours, @fusion_project.contractual_hours]
        @description << "Next Rollover Hours = (%f)"%
          [@next_rollover_hours]
      else
        @description << "[the sum of hours booked against Standard Business Hours (%f)] <= [Contractual Hours (%f)]" %
          [@standard_hours, @fusion_project.contractual_hours]
        @next_rollover_hours = (@fusion_project.contractual_hours - @standard_hours).to_f
        @description << "Next Rollover Hours (%f) = [Contractual Hours (%f)] - [the sum of hours booked against Standard Business Hours (%f)]"%
          [@next_rollover_hours, @fusion_project.contractual_hours, @standard_hours]
      end
      final_charge = method_b(extra_charge)
      @description << "Create the Bill with one line [Contractual Hours (%f)] * [Fixed Contractual Hourly Rate (%f)] PLUS n instances of, [Hourly Rate] * hours, where n is number of cases of ![Standard Business Hours] booked. Extra charge (%f)."%
          [@fusion_project.contractual_hours, @fusion_project.fixed_contractual_hourly_rate, extra_charge]
    end
    @next_rollover_hours = 0.0 if @next_rollover_hours < 0.0
    final_charge
    
    @charge.total
  end
  
  
  # Method A: Create the Bill with one line 
  # [Contractual Hours] * [Fixed Contractual Hourly Rate]
  def method_a
    charge = @fusion_project.contractual_hours * @fusion_project.fixed_contractual_hourly_rate
    @charge.add("Contract Hours", @fusion_project.contractual_hours, @fusion_project.fixed_contractual_hourly_rate, :standard)
    
    if @fusion_project.additional_service_fees > 0.0
      @description << "Add additional service fees (%f)" % [@fusion_project.additional_service_fees]
      charge += @fusion_project.additional_service_fees
      @charge.add_charge("Additional Service Fee", @fusion_project.additional_service_fees, :additional_service_fee) 
    end
    
    charge
  end
  
  # Method B: Create the Bill with one line 
  # [Contractual Hours] * [Fixed Contractual Hourly Rate] PLUS n instances of, 
  # [Hourly Rate] * hours, where n is number of cases of ![Standard Business Hours] booked
  def method_b extra_charge
    method_a + extra_charge
  end
  
end


