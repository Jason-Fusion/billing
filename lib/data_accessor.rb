
class DataAccessor

  CONTRACT_TYPE_SUPPORT = 49
  CONTRACT_TYPE_DELIVERY = 53
  CONTRACT_TYPE_INVOICE = 62

  attr_reader :fusion_projects, \
              :fusion_project, \
              :time_entries

  def initialize(project_id, context)
    @project = Project.find(project_id)
    @context = context
  end

  def setup
    unless is_this_bottom_layer_project?
      @context.set_not_bottom_project
      return false
    end

    @fusion_project.finance_issue = get_finance_issue
    @fusion_project.last_invoice_number = get_last_invoice_number

    setup_project_details

    @fusion_project.itms_rate = get_itms_rate
    @context.add_debug "ITMS/SS rate is %f", @fusion_project.itms_rate

    return true
  end

  def grab_data
    get_issues
    get_time_entries
  end

  private
  # Sow Calculator executes when itself is bottom project.
  def is_this_bottom_layer_project?
    query = <<-EOF
      select id, name, identifier from projects
      where status = 1 and lft >= #{@project.lft} and rgt <= #{@project.rgt}
      order by id, lft, rgt
    EOF

    subprojects = Project.select(:id, :name, :identifier).where("status = ? and lft >= ? and rgt <= ?", 1, @project.lft, @project.rgt).order(:id, :lft, :rgt)

    @fusion_projects = []

    subprojects.each do |e|
      @context.add_debug "Project ID: %d Name: %s Identiier:%s", \
                                           e["id"], e["name"], e["identifier"]
      if @project.id == e["id"]
        @fusion_project = FsgProject.new(e["id"], e["name"], e["identifier"])
      else
        @fusion_projects << FsgProject.new(e["id"], e["name"], e["identifier"])
      end
    end

    if @fusion_projects.size > 0
      return false
    end
    return true
  end # end method

  def get_finance_issue
    # Grab finance issuce's projects
    # i.g. Finance Tokyo...
    finance_project_ids = []
    # Project.find(:all, :conditions => [ 'name like ?', "Finance%" ]).each do |project|
    Project.where("name like ?", "Finance%").each do |project|
      finance_project_ids << project.id
    end

    # finance_issue = Issue.find(:first, :conditions => { :subject => @fusion_project.name, :project_id => finance_project_ids, :tracker_id => [CONTRACT_TYPE_SUPPORT, CONTRACT_TYPE_DELIVERY] })
    finance_issue = Issue.where(subject: @fusion_project.name, project_id: finance_project_ids, tracker_id: [49, 53]).first

    if finance_issue == nil
      raise "Format Error. Not found Finance Issue."
    end

    @context.add_debug "Fincance Issue ID: %d Name: %s Tracker ID: %d", finance_issue.id, finance_issue.subject, finance_issue.tracker_id
    return finance_issue
  end

  def get_last_invoice_number
    invoice_number = ""

    # Grab finance issuce's projects
    # i.g. Finance Tokyo...
    finance_project_ids = []
    # Project.find(:all, :conditions => [ 'name like ?', "Finance%" ]).each do |project|
    Project.where("name like ?", "Finance%").each do |project|
      finance_project_ids << project.id
    end

    invoice_name = "#{@fusion_project.name} [#{@context.base_date.strftime("%Y/%m")}]"

    p invoice_name
    # TODO Only Monthly now. No weelky's implementation
    # invoice_issue = Issue.find(:first, :conditions => { :subject => invoice_name, :project_id => finance_project_ids, :tracker_id => [CONTRACT_TYPE_INVOICE] })

    invoice_issue = Issue.where(subject: invoice_name, project_id: finance_project_ids, tracker_id: [62]).first

    unless invoice_issue == nil
      # CustomValue.find(:all, :conditions => { :customized_id => [invoice_issue.id]}).each do |cv|
      CustomValue.where(customized_id: [invoice_issue.id]).each do |cv|
        if cv.custom_field_id == 230 # Hard cording - Invoice Number
          invoice_number = cv.value unless cv.value == nil
        end
      end
      @context.add_debug "Fincance Issue ID: %d Name: %s Tracker ID: %d", invoice_issue.id, \
                                         invoice_issue.subject, invoice_issue.tracker_id
    end
    p invoice_issue
    return invoice_number
  end

  def setup_project_details

    finance_issue = @fusion_project.finance_issue
    @fusion_project.project_start_date = finance_issue.start_date
    @fusion_project.project_due_date = finance_issue.due_date
    @fusion_project.start_month = finance_issue.start_date.change(:day => 1) rescue Date.today.change(:day => 1)
    @fusion_project.due_month = finance_issue.due_date.change(:day => 1) rescue Date.today.change(:day => 1)
    @flag_74 = false    # 2014-09-12 added by Ochiai

    custom_field_hashmap = {}
    CustomField.all.each do |cf|
      custom_field_hashmap[cf.id] = cf.name
    end

    # CustomValue.find(:all, :conditions => { :customized_id => [finance_issue.id]}).each do |cv|
    CustomValue.where(customized_id: [finance_issue.id]).each do |cv|
      if cv.custom_field_id == 117 # Hard cording
        if cv.value == "Fixed" or cv.value == "Client Site"
          @fusion_project.type = FsgProject::CONTRACT_TYPE_FIXED
        elsif cv.value == "Maintenance"
          @fusion_project.type = FsgProject::CONTRACT_TYPE_MAINTENANCE
        elsif cv.value == "T&M"
          @fusion_project.type = FsgProject::CONTRACT_TYPE_TM

        #TODO add other types
        else
          @context.add_warning "No implementaion for contract type: %s for %d: %s. Just show accumulated time entries." % [cv.value, finance_issue.id, finance_issue.subject]
          #      [cv.value, finance_issue.id, finance_issue.subject]
          @fusion_project.type = FsgProject::CONTRACT_TYPE_MISCELLANEOUS
          #raise "Format Error. No implementaion for contract type: %s in %d: %s." % \
          #      [cv.value, finance_issue.id, finance_issue.subject]
        end

        @fusion_project.original_sow_type = cv.value
      end


      # The case only selected project has contractual hours.
      # Set contractual hours and rate into graph obj here so that label top area is shown.
      @fusion_project.contractual_hours = cv.value.to_f if cv.custom_field_id == 118
      @fusion_project.fixed_contractual_hourly_rate = cv.value.to_f if cv.custom_field_id == 136
#      # Client is FSG. We offer discount rate for them.
#      @fusion_project.fusion_system_group = true if cv.custom_field_id == 166 and cv.value != nil and cv.value != ""

      # Rollover
#      @fusion_project.last_month_rollover_hours = cv.value.to_f if cv.custom_field_id == 191
#      @fusion_project.this_month_rollover_hours = cv.value.to_f if cv.custom_field_id == 192
      @fusion_project.last_month_rollover_hours = cv.value.to_f if cv.custom_field_id == 239
      @fusion_project.this_month_rollover_hours = cv.value.to_f if cv.custom_field_id == 238

      # Fixed Value
      @fusion_project.value_to_fusion = cv.value.to_f if cv.custom_field_id == 89

      # Maintenance
      @fusion_project.additional_service_fees = cv.value.to_f if cv.custom_field_id == 131

      # Weekly Invoiced
      @fusion_project.weekly_invoiced = true if cv.custom_field_id == 215 and cv.value == '1'

      # Location
      if cv.custom_field_id == 91
        if cv.value.to_s == "TK"
          @fusion_project.location = FsgProject::LOCATION_TK
        elsif cv.value.to_s == "HK"
          @fusion_project.location = FsgProject::LOCATION_HK
        elsif cv.value.to_s == "SH"
          @fusion_project.location = FsgProject::LOCATION_SH
        elsif cv.value.to_s == "LA"
          @fusion_project.location = FsgProject::LOCATION_LA
        end
      end

      # Client Address # ... 2014-09-03 added by Ochiai (BEGIN)
      if cv.custom_field_id == 74
         @flag_74 = true
         if cv.value == nil
            # @fusion_project.client_address = "Address is set to Null for issue No."+ @fusion_project.finance_issue.id.to_s
            @fusion_project.client_address = ""
         elsif
            @fusion_project.client_address = cv.value.to_s
         end
      end
      # Client Address # ... 2014-09-03 added by Ochiai (END)

      # Using Hash and inserting data such as Meta programming.
      @fusion_project.custom_value[custom_field_hashmap[cv.custom_field_id]] = cv.value
    end

    # Client Address # ... 2014-09-12 added by Ochiai (BEGIN)
    if @flag_74 == false
      # @fusion_project.client_address = "Address is not registerd for issue No."+ @fusion_project.finance_issue.id.to_s
      @fusion_project.client_address = ""
    end
    # Client Address # ... 2014-09-12 added by Ochiai (END)

    if @fusion_project.tm? and @fusion_project.contractual_hours == 0.0
      @fusion_project.type = FsgProject::CONTRACT_TYPE_TM_SIMPLE
    end

    if @fusion_project.custom_value["Synthetic"] == "1" and @fusion_project.custom_value["FSG Billable Rate"] != nil
      # Client is FSG. We offer discount rate for them.
      @fusion_project.fusion_system_group = true
      @fusion_project.fusion_system_group_rate = @fusion_project.custom_value["FSG Billable Rate"].to_f
    end


    if @fusion_project.project_start_date == nil
      raise "Not found Start Date in Finance Ticket."
    end

    @context.add_debug "Project Details Project Start date: %s", @fusion_project.project_start_date
    @context.add_debug "Project Details Start Month date: %s", @fusion_project.start_month
    @context.add_debug "Project Details Due Month date: %s", @fusion_project.due_month
    @context.add_debug "Project Details Type: %s", @fusion_project.type
    @context.add_debug "Project Details Fixed -----"
    @context.add_debug "Project Details Value to Fusion: %f", @fusion_project.value_to_fusion
    @context.add_debug "Project Details Maintenance -----"
    @context.add_debug "Project Details Additional Service Fees: %f", @fusion_project.additional_service_fees
    @context.add_debug "Project Details T&M -----"
    @context.add_debug "Project Details Contractual Hours: %f", @fusion_project.contractual_hours if @fusion_project.contractual_hours != nil
    @context.add_debug "Project Details Fixed Contractual Hourly Rate: %f", @fusion_project.fixed_contractual_hourly_rate if @fusion_project.fixed_contractual_hourly_rate != nil
    @context.add_debug "Project Details Weekly Invoiced: %s", @fusion_project.weekly_invoiced
    @context.add_debug "Project Details Fusion Gruop? -----"
    @context.add_debug "Project Details Fusion System Group: %s", @fusion_project.fusion_system_group
    @context.add_debug "Project Details Custom Values -----"
    @fusion_project.custom_value.each do |k, v|
      @context.add_debug "Project Details Custom Value Key: %s Value: %s", k, v.to_s
    end
  end

  # Grab ITMS or SS rate from specific finance isssue
  def get_itms_rate
    itms_rate = 0.0
    if @fusion_project.location == FsgProject::LOCATION_TK
      # issue = Issue.find(:first, :conditions => { :subject => "TK - ITMS" })
      issue = Issue.where(subject: "TK - ITMS").first
      #CustomValue.find(:all, :conditions => { :customized_id => [issue.id]}).each do |cv|
      CustomValue.where(customized_id: [issue.id]).each do |cv|
        itms_rate = cv.value.to_f if cv.custom_field_id == 190
      end
      return itms_rate
    elsif @fusion_project.location == FsgProject::LOCATION_HK
      # issue = Issue.find(:first, :conditions => { :subject => "HK - ITMS" })
      issue = Issue.where(subject: "HK - ITMS").first
      # CustomValue.find(:all, :conditions => { :customized_id => [issue.id]}).each do |cv|
      CustomValue.where(customized_id: [issue.id]).each do |cv|
        itms_rate = cv.value.to_f if cv.custom_field_id == 190
      end
      return itms_rate
    elsif @fusion_project.location == FsgProject::LOCATION_SH
      # issue = Issue.find(:first, :conditions => { :subject => "SH - ITMS" })
      issue = Issue.where(subject: "SH - ITMS").first
      # CustomValue.find(:all, :conditions => { :customized_id => [issue.id]}).each do |cv|
      CustomValue.where(customized_id: [issue.id]).each do |cv|
        itms_rate = cv.value.to_f if cv.custom_field_id == 190
      end
      return itms_rate
    elsif @fusion_project.location == FsgProject::LOCATION_LA
      # issue = Issue.find(:first, :conditions => { :subject => "LA - ITMS" })
      issue = Issue.where(subject: "LA - ITMS").first
      # CustomValue.find(:all, :conditions => { :customized_id => [issue.id]}).each do |cv|
      CustomValue.where(customized_id: [issue.id]).each do |cv|
        itms_rate = cv.value.to_f if cv.custom_field_id == 190
      end
      return itms_rate
    end

    raise "Format Error. Not found ITMS/SS rate."
  end


  def get_issues
    # Make hashmap!! Key is Issue ID and Value is Issue object.
    @issue_hashmap = {}
    # Issue.find(:all, :conditions => {:project_id => @fusion_project.id}).each do |issue|
    Issue.where(project_id: @fusion_project.id).each do |issue|
      @issue_hashmap[issue.id] = issue
    end

  end # end method

  def get_time_entries
    # Grap all Time Enteries in selected project's.
    # time_entries_tmp = TimeEntry.find(:all, :include => [:user], :conditions =>["project_id = ? and spent_on >= ? ", @fusion_project.id, @fusion_project.project_start_date], :order => 'spent_on asc' )
    time_entries_tmp = TimeEntry.includes(:user).where("project_id = ? and spent_on >= ?", @fusion_project.id, @fusion_project.project_start_date).order(:spent_on)

    # Make Enumerations Hashmap!!
    enumeration_hashmap = {}
    enumerations = Enumeration.all
    enumerations.each do |enumeration|
      enumeration_hashmap[enumeration.id] = enumeration.name
    end

    @custom_value_89_hashmap = {}  # [Issue Id] => return Amount or Client rate
                                   # 89: there is value of Fixed Amount or Client rate
    @custom_value_117_hashmap = {} # [Issue Id] => return T&M or Fixed
                                   # 117: you can figure out T&M or Fixed
    @custom_value_101_hashmap = {} # [Issue Id] => return dispatch fee

    # Make Custom Values hashmap!!
    # custom_values = CustomValue.find(:all, :conditions => { :custom_field_id => [89, 117, 101]})
    custom_values = CustomValue.where(custom_field_id: [89, 117, 101])
    custom_values.each do |custom_value|
      @custom_value_89_hashmap[custom_value.customized_id] = custom_value \
                                      if custom_value.custom_field_id == 89
      @custom_value_117_hashmap[custom_value.customized_id] = custom_value \
                                      if custom_value.custom_field_id == 117
      @custom_value_101_hashmap[custom_value.customized_id] = custom_value \
                                      if custom_value.custom_field_id == 101
    end

    @time_entries = []

    # Mix time entries and enumerations as final time entries.
    # We prepared new class as Custom Time Entry.
    time_entries_tmp.each do |tmp_te|
      issue = @issue_hashmap[tmp_te.issue_id]

      unless issue == nil
        activity = enumeration_hashmap[tmp_te.activity_id]

        itms_rate = @fusion_project.itms_rate

        time_entry = FsgTimeEntry.new(tmp_te, \
                                      @fusion_project.type, \
                                      enumeration_hashmap[tmp_te.activity_id], \
                                      issue.tracker_id)

      # This logic is for LA transaction only
#      if time_entry.activity == "Travel" and @fusion_project.location == FsgProject::LOCATION_LA
#        # make a clone from original time entry record.
#        time_entry.commute = true
#      end

        @time_entries << time_entry

        # If dispatch is ticked, add time entry as one of a record for dispatch fee here.
        if @custom_value_101_hashmap.key? time_entry.id and @custom_value_101_hashmap[time_entry.id].value == "1"
          # make a clone from original time entry record.
          time_entry_dispatch = time_entry.clone
          time_entry_dispatch.dispatch = true
          time_entry_dispatch.comments = "[Dispatch Fee] " + time_entry_dispatch.comments
          time_entry_dispatch.hours = 1.0
          @time_entries << time_entry_dispatch
        end

      else
        @context.add_warning "Time Entry(%s) has no ISSUE ID and Tracker ID wasn't resolved.", tmp_te.id
      end

    end

    @time_entries.each do |time_entry|
      @context.add_debug time_entry.str
    end
  end
end


