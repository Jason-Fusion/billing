class Context
  OUTPUT_TYPE_NOT_BOTTOM_PROJECT = :not_bottom_project
  OUTPUT_TYPE_ERROR = :error_occured
  
  attr_reader :output_type, :debug, :warning, :error, :base_date
  attr_accessor :last_time, :this_time, :print, :client_restrict
  
  def initialize(base_date=Date.today, base_date_spacify = false, debug_mode = false)
    @debug = []
    @warning = []
    @error = []
    @output_type = nil
    #@today = Date.today
    @base_date = base_date
    @print = false
    @client_restrict = false
    @base_date_spacify = base_date_spacify
    @debug_mode = debug_mode
  end
  
  def add_debug(message, *args)
    @debug << message % args
  end
  
  def add_warning(message, *args)
    @warning << message % args
  end
  
  def add_error(message, *args)
    @error << message % args
  end
  
  def debug_mode?
    @debug_mode
  end
  
  def set_not_bottom_project 
    @output_type = Context::OUTPUT_TYPE_NOT_BOTTOM_PROJECT
  end

  def set_error_happen 
    @output_type = Context::OUTPUT_TYPE_ERROR
  end
  
  def base_date_spacify?
    @base_date_spacify
  end
end


