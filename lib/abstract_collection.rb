class AbstractCollection < Array

  attr_reader                                                                  \
    :tracker_id, :time_entries, :total_delivery_values, :billable_total_hours, \
    :nonbillable_total_hours, :times, :billable_stock, :nonbillable_stock,     \
    :client_rate_stock, :itms_rate_stock, :fixed, \
    :billable_ratio, \
    :nonbillable_ratio, :charge
  
  def initialize(fusion_project)
    @billable_total_hours = 0.0
    @nonbillable_total_hours = 0.0
    
    @revenue_sum = 0.0
    @expense_sum = 0.0
    
    @client_rate_stock = []
    @itms_rate_stock = []
    
    @billable_stock = {}
    @nonbillable_stock = {}
    @billable_detail_stock = {}
    
    @fusion_project = fusion_project

    @charge = Charge.new
  end # end method
  
  def add(time_entry)
    self << time_entry
    
    @revenue_sum += time_entry.revenue
    @expense_sum += time_entry.expense
    @billable_total_hours += time_entry.billable_hours
    @nonbillable_total_hours += time_entry.nonbillable_hours
    
    billable_detail time_entry
    set_rates time_entry
  end
  
  def revenue
    return @revenue_sum
  end # end method
  
  def expense
    return @expense_sum
  end # end method
   
  def profit
    return revenue - expense 
  end # end method
  
  def total_hours
    return @billable_total_hours + @nonbillable_total_hours
  end # end method
  
  def issue_date
    @end_date
  end

  def finalilze
    @total_hours = total_hours
    @billable_ratio = (@billable_total_hours / @total_hours * 100.0).round(0) rescue 0.0
    @nonbillable_ratio = 100.0 - @billable_ratio
#    @billable_stock.each_value do |value|
#      #value[:ratio] = @billable_ratio * value[:hours] / @billable_hours
#    end
#    @nonbillable_stock.each_value do |value|
#      #value[:ratio] = @nonbillable_ratio * value[:hours] / @nonbillable_hours
#    end
  end
  
  def str
    "Revenue %f Expense %f Profit %f Total Billable Hours %f Total NonBillable Hours %f" % \
         [revenue, expense, profit, @billable_total_hours, @nonbillable_total_hours]
  end

=begin 
  def comp
    date_to_month = {}
    self.sort.each_with_index do |val, index|
      if date_to_month.key?(val.spent_on)
        date_to_month[val.spent_on] += val.hours
      else
        date_to_month[val.spent_on] = val.hours
      end
    end
    
    date_to_month.sort_by{|key,val| key}.each do |val|
      p val
      yield val.first, val.last
    end
  end
=end

  # Sort time entries by spend on.
  def sort
    self.sort! do |a, b|
      (a.spent_on <=> b.spent_on).nonzero? ||
      (a.user_name <=> b.user_name).nonzero? ||
      (a.tracker_id <=> b.tracker_id).nonzero? ||
      (a.activity_id <=> b.activity_id).nonzero? ||
      (a.comments <=> b.comments)
    end
  end
  
  private
  def billable_detail time_entry
    
    if time_entry.billable?
      if @billable_stock.has_key? time_entry.activity
        @billable_stock[time_entry.activity] += time_entry.hours
      else
        @billable_stock[time_entry.activity] = time_entry.hours
        @billable_detail_stock[time_entry.activity] = []
      end
      # add for charging
      @billable_detail_stock[time_entry.activity] << time_entry
    else
      if @nonbillable_stock.has_key? time_entry.activity
        @nonbillable_stock[time_entry.activity] += time_entry.hours
      else
        @nonbillable_stock[time_entry.activity] = time_entry.hours
      end
    end
  end
  
  def set_rates time_entry
    unless time_entry.tracker_id == 53 or @client_rate_stock.include? time_entry.client_rate \
           or time_entry.billable? == false
      @client_rate_stock << time_entry.client_rate
    end
    
    unless @itms_rate_stock.include? time_entry.itms_rate
      @itms_rate_stock   << time_entry.itms_rate
    end
  end
end

