class MonthlyTmSimpleCollection < AbstractTmSimpleCollection
  ISSUE_DAY = 8



  def initialize(fusion_project, base_month)
    super(fusion_project)
    @beginning_date = base_month.beginning_of_month
    @end_date = base_month.end_of_month
  end

#  def issue_date
#    @beginning_date.change(:day => ISSUE_DAY)
#  end

  def provisional_version?(date)
    if date.day < ISSUE_DAY and (date.beginning_of_month << 1) == @beginning_date
      return true
    end
    return false
  end
end


