# This class has project information that fujion must need
class FsgProject
  CONTRACT_TYPE_TM = :tm
  CONTRACT_TYPE_TM_SIMPLE = :tm_simple
  CONTRACT_TYPE_FIXED = :fixed
  CONTRACT_TYPE_MAINTENANCE = :maintenance
  CONTRACT_TYPE_MISCELLANEOUS = :miscellaneous
  LOCATION_TK = :tk
  LOCATION_SH = :sh
  LOCATION_HK = :hk
  LOCATION_LA = :la
  
  attr_accessor :type, \
                :value_to_fusion, \
                :custom_value, \
                :finance_issue, \
                :project_start_date, \
                :project_due_date, \
                :start_month, \
                :due_month, \
                :contractual_hours, \
                :fixed_contractual_hourly_rate, \
                :fusion_system_group, \
                :custom_value, \
                :location, \
                :itms_rate, \
                :last_month_rollover_hours, \
                :this_month_rollover_hours, \
                :additional_service_fees, \
                :weekly_invoiced, \
                :fusion_system_group_rate, \
                :original_sow_type, \
                :last_invoice_number, \
                :client_address              # 2014-09-03 added by Ochiai
  
  attr_reader   :id, :name, :identifier, :client_name
  
  def initialize(id, name, identifier)
    @id = id
    @name = name
    @contractual_hours = 0.0
    @fixed_contractual_hourly_rate = 0.0
    @value_to_fusion = 0.0
    @additional_service_fees = 0.0
    @identifier = identifier
    @custom_value = {}
    @fusion_system_group = false
    @last_month_rollover_hours = 0.0
    @this_month_rollover_hours = 0.0
    @client_name =  @name.split(":").shift
    @weekly_invoiced = false
    @fusion_system_group_rate = 0.0
  end
  
  def uri_charts(target="billing")
    return "../#{@identifier}/#{target}"
  end
  
  def fusion_system_group?
    @fusion_system_group
  end
  
  def tm?
    @type == FsgProject::CONTRACT_TYPE_TM rescue false
  end
  
  def tm_simple?
    @type == CONTRACT_TYPE_TM_SIMPLE rescue false
  end

  def fixed?
    @type == FsgProject::CONTRACT_TYPE_FIXED rescue false
  end
  
  def maintenance?
    @type == FsgProject::CONTRACT_TYPE_MAINTENANCE rescue false
  end

  def miscellaneous?
    @type == FsgProject::CONTRACT_TYPE_MISCELLANEOUS rescue false
  end
  
  def weekly_invoiced?
    @weekly_invoiced
  end


  def get_term(start=@start_month, due=@due_month)
    return 1 if start.year == due.year and start.month == due.month
    24.times do |i|
      start = start >> 1
      return i + 2 if start.year == due.year and start.month == due.month
    end
    return 24
  end # end method
end





