class MiscellaneousSowCalculator < MonthlyTmSimpleSowCalculator

  private
  def make_collection
    monthly_hash = {
      @last_one => MiscellaneousCollection.new(@fusion_project, @last_one),
      @this_one => MiscellaneousCollection.new(@fusion_project, @this_one),
    }
    
    
    @data.time_entries.each do |time_entry|
      entry_month = time_entry.spent_on.change(:day => 1)
      
      if monthly_hash.has_key? entry_month
        monthly_hash[entry_month].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end
    
    monthly_hash
  end

end



