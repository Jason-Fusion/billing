
class AbstractTmSimpleCollection < AbstractTmCollection

  def initialize(fusion_project)
    super(fusion_project)
    
  end
  
  def provisional_version?
    raise "Need to implement this method on sub class"
  end
  
  # Always return 0. This is used for Web Service to generate invoice ticket by script
  def next_rollover_hours
    0
  end
  
  def finalilze()
    super()
    @final_charge = charge_to_client
  end
  
  private
  def charge_to_client
    
    #final_charge = 0.0
    @billable_detail_stock.each do |key, time_entries|
      if STANDARD_BUSINESS_HOURS_LIST.include? key
        time_entries.each do |time_entry|
          @standard_hours += time_entry.hours
          @charge.add(time_entry.activity, time_entry.hours, time_entry.client_rate, :standard)
        end
      else
        time_entries.each do |time_entry|
          @nonstandard_hours += time_entry.hours
          @charge.add(time_entry.activity, time_entry.hours, time_entry.client_rate, :nonstandard)
        end
      end
      

    end

    if @fusion_project.additional_service_fees > 0.0
      @charge.add_charge("Monthly Additional Service Fee", @fusion_project.additional_service_fees, :maintenance)
    end
    
    @charge.total
  end
  
end


