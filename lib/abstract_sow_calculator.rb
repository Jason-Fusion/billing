class AbstractSowCalculator
  attr_reader :collection_hash
  
  def initialize(data, context)
    @data = data
    @fusion_project = @data.fusion_project
    @context = context
  end # end method
  
  def setup
    raise "Need to implement this method on sub class"
  end# end method
  
  def calc
    raise "Need to implement this method on sub class"
  end # end method
  
  # Return last month or week collection instance data
  def get_last_collection
    raise "Need to implement this method on sub class"
  end
  
  # Return this month or week collection instance data
  def get_this_collection
    raise "Need to implement this method on sub class"
  end
  
  # Return bool if this collection is Provisional Version or not
  def provisional?
    raise "Need to implement this method on sub class"
  end


  # Return last month or week csv data
  def generate_csv()
    require 'fastercsv'
    collection = get_last_collection
    
    csv = FasterCSV.generate("") do |csv_row|
      csv_row << [
                   "Client",
                   "Project",
                   "Date",
                   "In Charge",
                   "Tracker",
                   "Activity",
                   "Comments",
                   "Hours",
                   "Rate",
                   "Hours x Rate"
                 ]
                 
      collection.each do |time_entry|
        if time_entry.billable?
          csv_row << [
                     @fusion_project.client_name,
                     @fusion_project.name,
                     time_entry.spent_on.strftime("%d %b %Y"), 
                     time_entry.user_name, 
                     time_entry.tracker, 
                     time_entry.activity, 
                     time_entry.comments, 
                     time_entry.hours, 
                     time_entry.client_rate, 
                     time_entry.revenue
                   ]
        end    
      end
    end
    csv
  end
  
  private
  def get_term(start, due)
    return 1 if start.year == due.year and start.month == due.month
    24.times do |i|
      start = start >> 1
      return i + 2 if start.year == due.year and start.month == due.month
    end
    return 24
  end # end method
end



