class FixedSowCalculator < AbstractSowCalculator
  attr_reader :collection_hash
  attr_reader :project_total_hours

  def setup
    if @fusion_project.value_to_fusion == 0.0
      raise "No value to fusion setting in Fincance ticket"
    end

    @today = @context.base_date


    @last_month = @context.base_date.change(:day => 1) << 1
=begin
    @this_month = @context.base_date.change(:day => 1)
    @context.last_time = @last_month
    @context.this_time = @this_month
=end
    @project_total_hours = 0.0
  end# end method

  def calc
    calc_charge_maintenance
    @collection_hash = monthly_calc

    @collection_hash.each_value do |collection|
      collection.finalilze
    end
  end # end method

  # Return bool if this collection is Provisional Version or not
  def provisional?
  end

  private
  def monthly_calc
    monthly_hash = {}

    (@fusion_project.start_month..@fusion_project.due_month).each do |a_day|
      if a_day.day == 1
        monthly_hash[a_day] = FixedCollection.new(@fusion_project, @last_month)
      end
    end

    @data.time_entries.each do |time_entry|
      entry_month = time_entry.spent_on.change(:day => 1)
      if monthly_hash.has_key? entry_month
        monthly_hash[entry_month].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end

    monthly_hash.each do |key, value|
      @project_total_hours += value.total_hours
    end

    monthly_hash
  end

  def calc_charge_maintenance
    @data.time_entries.each do |time_entry|
      time_entry.itms_rate = @fusion_project.itms_rate
      time_entry.calc_charge
    end
  end

end



