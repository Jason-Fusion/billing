class WeeklyTmContractHoursSowCalculator < TmContractHoursSowCalculator
  
  # Override parent class method
  def setup
    @today = @context.base_date
    @this_one = @context.base_date.beginning_of_week
    @last_one = (@context.base_date - 1.week).beginning_of_week
    @context.last_time = @last_one
    @context.this_time = @this_one
  end

  
  # Override checking provisional date or not. However for LA I don't 
  # know this logic is the best.
  def provisional?
    if @today.wday == 1 and @today.wday == 2
      return true
    end
    return false
  end
  
  private
  def make_collection
    weekly_hash = {
      @last_one => WeeklyTmContractHoursCollection.new(@fusion_project, @last_one),
      @this_one => WeeklyTmContractHoursCollection.new(@fusion_project, @this_one),
    }
    
    @data.time_entries.each do |time_entry|
      #entry_monday = time_entry.spent_on - (time_entry.spent_on.wday - 1)
      entry_monday = time_entry.spent_on.beginning_of_week
      if weekly_hash.has_key? entry_monday
        weekly_hash[entry_monday].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end
    weekly_hash
  end
end

