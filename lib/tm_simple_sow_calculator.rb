class TmSimpleSowCalculator < AbstractSowCalculator

  def initialize(data, context)
    super(data, context)
    @last_one = nil
    @this_one = nil
  end

  def setup
  end# end method
  
  def calc
    calc_charge_tm
    
    @collection_hash = make_collection
    
    collect_by_unit_tm
  end # end method
  
  # Return bool if this collection is Provisional Version or not
  def provisional?

  end

  # Return last month or week collection instance data
  def get_last_collection
    return @collection_hash[@last_one]
  end
  
  # Return this month or week collection instance data
  def get_this_collection
    return @collection_hash[@this_one]
  end
  
  private
  def make_collection
  end
  def calc_charge_tm
    @data.time_entries.each do |time_entry|
      if time_entry.billable? 
#TODO No more commute, so delete this
=begin
        if time_entry.commute == true
          time_entry.client_rate = @fusion_project.fixed_contractual_hourly_rate
        elsif @fusion_project.custom_value.has_key? time_entry.activity and @fusion_project.custom_value[time_entry.activity] != ""
          time_entry.client_rate = @fusion_project.custom_value[time_entry.activity].to_f
        else
          raise "Configuration Error. Not found client rate: %s in finance tikect." % [time_entry.activity]
        end
=end
        if @fusion_project.custom_value.has_key? time_entry.activity and @fusion_project.custom_value[time_entry.activity] != ""
          time_entry.client_rate = @fusion_project.custom_value[time_entry.activity].to_f
        else
          raise "Configuration Error. Not found client rate: %s in finance tikect." % [time_entry.activity]
        end
      end
      time_entry.itms_rate = @fusion_project.itms_rate
      time_entry.calc_charge
    end
  end
  
  def collect_by_unit_tm
    [@last_one, @this_one].each do |key|
      collection = @collection_hash[key]
      
      collection.finalilze 
    end
  end
end



