class MonthlyTmSimpleSowCalculator < TmSimpleSowCalculator

  def setup
    @today = @context.base_date
    @last_one = @context.base_date.change(:day => 1) << 1
    @this_one = @context.base_date.change(:day => 1)
    @context.last_time = @last_one
    @context.this_time = @this_one
  end# end method

  # Return bool if this collection is Provisional Version or not
  def provisional?
    if 0 < @today.day and @today.day < 8
      return true
    end
    return false
  end

  private
  def make_collection
    monthly_hash = {
      @last_one => MonthlyTmSimpleCollection.new(@fusion_project, @last_one),
      @this_one => MonthlyTmSimpleCollection.new(@fusion_project, @this_one),
    }


    @data.time_entries.each do |time_entry|
      entry_month = time_entry.spent_on.change(:day => 1)

      if monthly_hash.has_key? entry_month
        monthly_hash[entry_month].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end

    monthly_hash
  end

=begin
  def calc_charge_tm
    @data.time_entries.each do |time_entry|
      if time_entry.billable?
        if time_entry.commute == true
          time_entry.client_rate = @fusion_project.fixed_contractual_hourly_rate
        elsif @fusion_project.custom_value.has_key? time_entry.activity and @fusion_project.custom_value[time_entry.activity] != ""
          time_entry.client_rate = @fusion_project.custom_value[time_entry.activity].to_f
        else
          raise "Configuration Error. Not found client rate: %s in finance tikect." % [time_entry.activity]
        end
      end
      time_entry.itms_rate = @fusion_project.itms_rate
      time_entry.calc_charge
    end
  end

  def collect_by_unit_tm
    [@last_one, @this_one].each do |key|
      collection = @collection_hash[key]

      collection.finalilze
    end
  end
=end
end


