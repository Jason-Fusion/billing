class MonthlyTmFsgSowCalculator < MonthlyTmSimpleSowCalculator

  private

  def calc_charge_tm
    @data.time_entries.each do |time_entry|
      if time_entry.billable? 
        time_entry.client_rate = @fusion_project.fusion_system_group_rate
      end
      time_entry.itms_rate = @fusion_project.itms_rate
      time_entry.calc_charge
    end
  end
end


