
class MaintenanceCollection < AbstractCollection
  ISSUE_DAY = 8

  attr_reader :minimum_charge, :rollover_hours, :next_rollover_hours, \
              :standard_hours, :nonstandard_hours, :final_charge, :description, \
              :billable_stock, :charge_lines
  attr_reader :beginning_date, :end_date

  def initialize(fusion_project, base_month)
    super(fusion_project)
    #@final_charge = charge
    #@charge = Charge.new
    @beginning_date = base_month.beginning_of_month
    @end_date = base_month.end_of_month
  end

  def add(time_entry)
    self << time_entry
    @billable_total_hours += time_entry.billable_hours
    @nonbillable_total_hours += time_entry.nonbillable_hours
  end

  # Override parent class
  def revenue
    return @final_charge
  end # end method


#  def issue_date
#    @beginning_date.change(:day => ISSUE_DAY)
#  end

  # Return bool if this collection is Provisional Version or not
  def provisional_version?(date)
    if date.day < ISSUE_DAY and (date.beginning_of_month << 1) == @beginning_date
      return true
    end
    return false
  end

  def has_billable?
    false
  end

  def finalilze()
    super()
    @final_charge = charge_to_client
  end

  private
  def charge_to_client
    #@charge.add("", @final_charge, :maintenance)
    #@charge_lines[:standard] = ["Service Fee", 0.0, 0.0, @final_charge]

p @fusion_project.additional_service_fees
    if @fusion_project.additional_service_fees > 0.0

      @charge.add_charge("Monthly Service Fee", @fusion_project.additional_service_fees, :maintenance)
    end

  end

end



