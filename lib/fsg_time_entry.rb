
# The TimeEntry object ActiveRecords generates has not enough imformation,
# not enough fields. So we use this class instead of TimeEntry object with
# additional fields.
class FsgTimeEntry
  DEFAULT_SUPPORT = %w( 49 11 9 52 6 43 13 4 )
  DEFAULT_DELIVERY = %w( 53 )
  DEFAULT_PRESALES_PROPOSAL = %w( 5 50 38 54 42 )
  
  attr_reader   :spent_on, :user_name, \
                :issue_id, :activity_id, :id, :project_id, :tracker_id, \
                :contract_type, \
                :activity
  attr_accessor :client_rate, \
                :itms_rate, \
                :comments, \
                :dispatch, \
                :commute, \
                :hours, \
                :revenue, \
                :expense, \
                :billable_hours, \
                :nonbillable_hours
  
  def str
    "User: %s Hours: %f Tracker: %s Activity: %s " % \
             [@user_name, @hours, tracker, @activity]
  end
  
  def str2
    "Clinet Rate: %f ITMS Rate: %f Billable: %s Revenue: %f Expense: %f" % \
              [@client_rate, @itms_rate, billable?, @revenue, @expense]
  end
  
  def initialize(time_entry, contract_type, activity, tracker_id)
    @contract_type = contract_type
    @spent_on    = time_entry.spent_on
    @user_name   = time_entry.user.login
    @comments    = time_entry.comments
    @hours       = time_entry.hours
    @issue_id    = time_entry.issue_id
    @activity_id = time_entry.activity_id
    @id          = time_entry.id
    @project_id  = time_entry.project_id
    @activity    = activity            # activity name
    @tracker_id  = tracker_id
    @dispatch    = false
    @commute     = false
    @client_rate = 0.0
    @itms_rate   = 0.0    
    @revenue     = 0.0
    @expense     = 0.0
    @billable_hours = 0.0
    @nonbillable_hours = 0.0
  end
  
  # Return whether this time entry is billable or non-billable.
  def billable?
    return is_billable
  end
  
  # Return tracker by text.
  def tracker
    return 	"Handling" 	if @tracker_id ==	4
    return 	"Pre-Sales" 	if @tracker_id ==	5
    return 	"Translation" 	if @tracker_id ==	6
    return 	"Client: Incident" 	if @tracker_id ==	8
    return 	"Problem" 	if @tracker_id ==	9
    return 	"Change" 	if @tracker_id ==	10
    return 	"Project" 	if @tracker_id ==	11
    return 	"Site Visit" 	if @tracker_id ==	13
    return 	"Research" 	if @tracker_id ==	14
    return 	"??" 	if @tracker_id ==	23
    return 	"Code" 	if @tracker_id ==	24
    return 	"Design" 	if @tracker_id ==	25
    return 	"SQA" 	if @tracker_id ==	26
    return 	"Task" 	if @tracker_id ==	27
    return 	"Recruiting" 	if @tracker_id ==	35
    return 	"Annual Reveiw" 	if @tracker_id ==	37
    return 	"Quote" 	if @tracker_id ==	38
    return 	"Bug" 	if @tracker_id ==	39
    return 	"Out of Office" 	if @tracker_id ==	42
    return 	"Management" 	if @tracker_id ==	43
    return 	"Support" 	if @tracker_id ==	49
    return 	"Proposal" 	if @tracker_id ==	50
    return 	"Interview" 	if @tracker_id ==	51
    return 	"Assignment" 	if @tracker_id ==	52
    return 	"Delivery" 	if @tracker_id ==	53
    return 	"PO" 	if @tracker_id ==	54
    return 	"Feature" 	if @tracker_id ==	55
    return  "Unknown: %s" % [@tracker_id]
  end
  
  def calc_charge
    if @contract_type == FsgProject::CONTRACT_TYPE_FIXED \
         or @contract_type == FsgProject::CONTRACT_TYPE_MAINTENANCE
      @expense = @itms_rate * @hours
      @billable_hours = @hours
    else 
      if @commute == true
        @revenue = @client_rate * @hours * 0.5
        @expense = @itms_rate * @hours
        @billable_hours = @hours
      elsif FsgTimeEntry::DEFAULT_SUPPORT.include?(@tracker_id.to_s)
        if billable?
          @revenue = @client_rate * @hours
          @billable_hours = @hours
        else
          @nonbillable_hours = @hours
        end
        @expense = @itms_rate * @hours
      elsif FsgTimeEntry::DEFAULT_DELIVERY.include?(@tracker_id.to_s)
        @expense = @itms_rate * @hours
        @nonbillable_hours = @hours
      elsif FsgTimeEntry::DEFAULT_PRESALES_PROPOSAL.include?(@tracker_id.to_s)
        @expense = @itms_rate * @hours
        @nonbillable_hours = @hours
      else
        @expense = @itms_rate * @hours
        @nonbillable_hours = @hours
      end
    end
  end
    
  private
  def is_billable
    if @contract_type == FsgProject::CONTRACT_TYPE_FIXED \
         or @contract_type == FsgProject::CONTRACT_TYPE_MAINTENANCE
      return true
    end
    
    return false if @activity =~ /Non billable/
    return false if @activity =~ /Waived/
    
    if DEFAULT_SUPPORT.include? @tracker_id.to_s
      return true  if @activity =~ /Travel/ and @commute
      return true  if @activity =~ /Billable/
      return true  if @activity =~ /Weekday/
      return true  if @activity =~ /Weekend/
      return true  if @activity =~ /Standard/
    end
    
    return false if @activity =~ /Travel/
    
    if DEFAULT_DELIVERY.include? @tracker_id.to_s
      return true  if @activity =~ /Fixed/
    end
    
    return false
  end

end


