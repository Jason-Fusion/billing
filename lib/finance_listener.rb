
require "./plugins/billing/app/helpers/billing_helper"

# Extend class of View hook
class FinanceListener < Redmine::Hook::ViewListener
  include BillingHelper

  # Show download link. You can download last month's billing from invoice issue. 
  def view_issues_show_description_bottom(context)
    issue = context[:issue]

    if issue.tracker_id == 62 #or issue.tracker_id == 49 or issue.tracker_id == 53 # Invoicing, 
      header = '<hr/><p><strong>Link</strong></p>'
      buff = ''
      buff << "Download Last Month Billing(Created by Redmine) as CSV: "
      buff2 = ''
      buff2 << "See Charts and Last Month Billing Records: "
      
      buff_billing = ''
      buff_billing << "Billing: "
      buff_invoice = ''
      buff_invoice << "Invoice: " 
      buff_csv = ''
      buff_csv << "CSV Download: " 
            
      table = ""
      table << "<b>Transactions</b><br/>"
      
      fixed_subject = issue.subject.clone

      fixed_subject.slice!(/\s\[\d{4}[\/@]\d{2}\]/)
#      fixed_subject.slice!(/\s\[\d{4}\/\d{2}\]/)

      project = Project.find(:first, :conditions => {:name => fixed_subject, :status => [1, 2, 3, 4, 5, 6, 7, 8]}, :order => "updated_on DESC") rescue nil
      
      if project != nil and project.identifier != nil
        link = "../projects/#{project.identifier}/charts.csv?start_date=" + issue.start_date.strftime("%Y-%m-%d")
        buff << link_to("Click Here", link, :method => :get)
        
        link2 = "../projects/#{project.identifier}/charts"
        buff2 << link_to("Click Here", link2, :method => :get)
        
        if issue.tracker_id == 62
          link_billing = "../projects/#{project.identifier}/billing?base_date=" + issue.start_date.strftime("%Y-%m-%d")
          buff_billing << link_to("Click Here", link_billing, :method => :get)
        
          link_invoice = "../projects/#{project.identifier}/invoice?base_date=" + issue.start_date.strftime("%Y-%m-%d")
          buff_invoice << link_to("Click Here", link_invoice, :method => :get)
        else
          link_billing = "../projects/#{project.identifier}/billing"
          buff_billing << link_to("Click Here", link_billing, :method => :get)
        
          link_invoice = "../projects/#{project.identifier}/invoice"
          buff_invoice << link_to("Click Here", link_invoice, :method => :get)
        end
        
        link_csv = "../projects/#{project.identifier}/billing.csv?base_date=" + issue.start_date.strftime("%Y-%m-%d")
        buff_csv << link_to("Click Here", link_csv, :method => :get)
        
        begin
        
          base_date = issue.start_date
          
          @context = Context.new(base_date)
          @data = DataAccessor.new(project.id, @context)
          
          ret = @data.setup
          
          if @data.fusion_project.tm?
          # In case of not bottom layer project
          raise "error" unless ret
    
          @data.grab_data
          
          # Need to be the same with BillingController's logic
          if @data.fusion_project.tm?
            if @data.fusion_project.location == FsgProject::LOCATION_LA and @data.fusion_project.weekly_invoiced?
              @billing = WeeklyTmContractHoursSowCalculator.new(@data, @context)
            else
              @billing = MonthlyTmContractHoursSowCalculator.new(@data, @context)
            end
          elsif @data.fusion_project.tm_simple?
            if @data.fusion_project.location == FsgProject::LOCATION_LA and @data.fusion_project.weekly_invoiced?
              @billing = WeeklyTmSimpleSowCalculator.new(@data, @context)
            else
              @billing = MonthlyTmSimpleSowCalculator.new(@data, @context)
            end
          elsif @data.fusion_project.maintenance? and @data.fusion_project.location == FsgProject::LOCATION_TK
            @billing = MaintenanceSowCalculator.new(@data, @context)
          elsif @data.fusion_project.fixed? and @data.fusion_project.location == FsgProject::LOCATION_TK
            @billing = FixedSowCalculator.new(@data, @context)
          elsif @data.fusion_project.miscellaneous?
            @billing = MiscellaneousSowCalculator.new(@data, @context)
          else
            raise "No implementaion for %s and %s." % \
              [@data.fusion_project.type, @data.fusion_project.location]
          end
          #          @billing = SowCalculator.new(@data, @context)
    
          @billing.setup
          @billing.calc

          #key_month = issue.start_date.months_ago(1).change(:day => 1)
          collection = @billing.get_last_collection
          
          table << transaction_table(collection)
          end
        rescue => e
          table << e.message
        end 
      else
        buff << "can not find racords"
        buff2 << "can not find racords"
        
      end
      
      return header + 
             #buff2 + "<br/>" + 
             #buff + "<br/><br/>" + 
             buff_billing + "<br/>" + 
             buff_invoice + "<br/>" + 
             buff_csv + "<br/><br/>" + 
             table
    elsif issue.tracker_id == 49
      header = '<hr/><p><strong>Link</strong></p>'
      
      tickets = Issue.where('subject like ?', "#{issue.subject} [%").order('created_on DESC') rescue nil
      
      buff = ''
      tickets.each do |ticket|
        link_invoice = "../issues/#{ticket.id}"
        buff << link_to(ticket.subject, link_invoice, :method => :get)
        buff << '<br/>'
      end
      
      return header + buff
    end
  end
  
  # Show notice when you create invoice issue.
  def view_issues_form_details_bottom(context={ })
    issue = context[:issue]
    
    #ChartsController.new.index
    
    if issue.tracker_id == 62 # Invoicing
      buff =  '<div align="center"><table style="width: 650px; border:1px solid #ff00ff;">'
      buff << "<tr><td>"
      buff << "[Notice] In the case of Invoicing"
      buff << "<ul>"
      buff << "  <li>Subject must be the same with the name of your project.</li>"
      buff << "  <li>Attach your invoice docuemnt here.</li>"
      buff << "</ul>"
      buff << "</td></tr>"
      buff << "</table><div>"
      
      return buff
    end
  end
end


