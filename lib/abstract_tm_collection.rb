
class AbstractTmCollection < AbstractCollection
  STANDARD_BUSINESS_HOURS_LIST = [ 
    "Weekday: 08:00-20:00", 
    "Weekday: 08:00-18:00", 
    "Weekday: 09:00-18:00",
    "Billable"
  ]

  attr_reader :final_charge, :description, :standard_hours, :nonstandard_hours
  attr_reader :beginning_date, :end_date
  
  def initialize(fusion_project)
    super(fusion_project)
 
    @final_charge = 0.0
    @standard_hours = 0.0
    @nonstandard_hours = 0.0
  end
  
  def finalilze()
    super()
  end
  
  def has_billable?
    return @billable_total_hours > 0.0 ? true : false
  end
  
  def provisional_version?
    raise "Need to implement this method on sub class"
  end
    
  private
  def charge_to_client
    raise "Need to implement this method on sub class"
  end

end


