class BillingHookListener < Redmine::Hook::ViewListener
  render_on :view_issues_show_description_bottom, :partial => 'billing/view_issues_show_description_bottom'
end
