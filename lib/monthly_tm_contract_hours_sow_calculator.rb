class MonthlyTmContractHoursSowCalculator < TmContractHoursSowCalculator

  def setup
    @today = @context.base_date

    @last_one = @context.base_date.change(:day => 1) << 1
    @this_one = @context.base_date.change(:day => 1)
    @context.last_time = @last_one
    @context.this_time = @this_one
  end# end method



  # Return bool if this collection is Provisional Version or not
  def provisional?
    if 0 < @today.day and @today.day < 8
      return true
    end
    return false
  end

  private
  def make_collection
    monthly_hash = {
      @last_one => MonthlyTmContractHoursCollection.new(@fusion_project, @last_one),
      @this_one => MonthlyTmContractHoursCollection.new(@fusion_project, @this_one),
    }

    @data.time_entries.each do |time_entry|
      entry_month = time_entry.spent_on.change(:day => 1)

      if monthly_hash.has_key? entry_month
        monthly_hash[entry_month].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end

    monthly_hash
  end


end




