class MaintenanceSowCalculator < AbstractSowCalculator

  def initialize(data, context)
    super(data, context)
  end

  def setup
    if @fusion_project.additional_service_fees == nil
      raise "No additional service fees setting in Fincance ticket"
    end

    @today = @context.base_date

    # TODO beginning date and due date
    @last_one = @context.base_date.change(:day => 1) << 1
    @this_one = @context.base_date.change(:day => 1)
    @context.last_time = @last_one
    @context.this_time = @this_one

  end# end method

#  def issue_date
#    @beginning_date.change(:day => ISSUE_DAY)
#  end

  # Return bool if this collection is Provisional Version or not
  def provisional_version?(date)
    if date.day < ISSUE_DAY and (date.beginning_of_month << 1) == @beginning_date
      return true
    end
    return false
  end

  # Return last month or week collection instance data
  def get_last_collection
    return @collection_hash[@last_one]
  end

  # Return this month or week collection instance data
  def get_this_collection
    return @collection_hash[@this_one]
  end

  def calc
    calc_charge_maintenance

    @collection_hash = monthly_calc

    @collection_hash.each_value do |collection|
      collection.finalilze
    end
  end # end method


  private
  def monthly_calc
    monthly_hash = {}

    (@last_one..@this_one).each do |a_day|
      if a_day.day == 1
        monthly_hash[a_day] = MaintenanceCollection.new(@fusion_project, @last_one)
      end
    end

    @data.time_entries.each do |time_entry|
      entry_month = time_entry.spent_on.change(:day => 1)
      if monthly_hash.has_key? entry_month
        monthly_hash[entry_month].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end

    monthly_hash
  end

  def calc_charge_maintenance
    @data.time_entries.each do |time_entry|
      time_entry.itms_rate = @fusion_project.itms_rate
      time_entry.calc_charge
    end
  end


end



