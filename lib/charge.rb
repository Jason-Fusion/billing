class Charge
  
  STANDARD_BUSINESS_HOURS = "Standard Business Hours"
  NON_STANDARD_BUSINESS_HOURS = "Non Standard Business Hours"
  ADDITIONAL_SERVICE_FEE = "Extra Fee"
  MAINTENANCE = "Service Fee"
  ETC = "Other Type"
  
  def initialize 
    @lines = {}
    @charge_is_finalize = false
    @total_charge = 0.0
    @valid = false
  end
  
  def add(title, hours, rate, type)
    raise "You can't add charge record anymore!!" if @charge_is_finalize
    @valid = true unless @valid
    
    if @lines.has_key? title
      @lines[title][:hours] += hours
    else
      @lines[title] = {:hours => hours, :rate => rate, :type => type}
    end
  end
  
  def add_charge(title, charge, type)
    raise "You can't add charge record anymore!!" if @charge_is_finalize

    # Round decimal
    charge = charge.round
    
    @valid = true unless @valid
    @lines[title] = {:hours => "-", :rate => "-", :subtotal => charge, :type => type}
  end
  
  def finalize
    @lines.each do |title, value|
      # calulate subtotal and round decimal
      value[:subtotal] = (value[:hours] * value[:rate]).round if value[:type] != :maintenance and value[:type] != :additional_service_fee
      @total_charge += value[:subtotal]
    end
    @charge_is_finalize = true
  end
  
  # e.g. charge.each { |title, subtitle, hours, rate, subtotal| do something }
  def each
    self.finalize unless @charge_is_finalize 
    @lines.each do |subtitle, value|
      if value[:type] == :standard or value[:type] == :extrastandard
        yield STANDARD_BUSINESS_HOURS, subtitle, value[:hours], value[:rate], value[:subtotal]
      elsif value[:type] == :nonstandard
        yield NON_STANDARD_BUSINESS_HOURS, subtitle, value[:hours], value[:rate], value[:subtotal]
      elsif value[:type] == :maintenance
        yield MAINTENANCE, subtitle, value[:hours], value[:rate], value[:subtotal]
      elsif value[:type] == :additional_service_fee
        yield ADDITIONAL_SERVICE_FEE, subtitle, value[:hours], value[:rate], value[:subtotal]
      else
        yield ETC, subtitle, value[:hours], value[:rate], value[:subtotal]
      end
    end
  end
  
  def total
    self.finalize unless @charge_is_finalize
    @total_charge.ceil
  end
  
  def valid?
    return @valid
  end
end
