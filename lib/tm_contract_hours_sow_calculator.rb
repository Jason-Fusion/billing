class TmContractHoursSowCalculator < AbstractSowCalculator


  def setup
  end# end method
  
  def calc
    calc_charge_tm
    
    @collection_hash = make_collection
    
    collect_by_unit_tm
  end # end method
  
  # Return bool if this collection is Provisional Version or not
  def provisional?

  end

  # Return last month or week collection instance data
  def get_last_collection
    return @collection_hash[@last_one]
  end
  
  # Return this month or week collection instance data
  def get_this_collection
    return @collection_hash[@this_one]
  end
  
  
  private
  def make_collection
    monthly_hash = {
      @last_one => MonthlyTmContractHoursCollection.new(@fusion_project, @last_one),
      @this_one => MonthlyTmContractHoursCollection.new(@fusion_project, @this_one),
    }
    
    @data.time_entries.each do |time_entry|
      entry_month = time_entry.spent_on.change(:day => 1)
      
      if monthly_hash.has_key? entry_month
        monthly_hash[entry_month].add(time_entry)
        @context.add_debug time_entry.str2
      end
    end
    
    monthly_hash
  end
  
  def calc_charge_tm
    @data.time_entries.each do |time_entry|
      if time_entry.billable? 
        if time_entry.commute == true
          time_entry.client_rate = @fusion_project.fixed_contractual_hourly_rate
        elsif @fusion_project.custom_value.has_key? time_entry.activity and @fusion_project.custom_value[time_entry.activity] != ""
          time_entry.client_rate = @fusion_project.custom_value[time_entry.activity].to_f
        else
          raise "Configuration Error. Not found client rate: %s in finance tikect." % [time_entry.activity]
        end
      end
      time_entry.itms_rate = @fusion_project.itms_rate
      time_entry.calc_charge
    end
  end
  
  def collect_by_unit_tm
    [@last_one, @this_one].each do |key|
      collection = @collection_hash[key]
      if key == @last_one
        if provisional?
          rollover_hours = @fusion_project.this_month_rollover_hours
        else
          rollover_hours = @fusion_project.last_month_rollover_hours
        end
      elsif key == @this_one
        if provisional?
             
          rollover_hours = @collection_hash[@last_one].next_rollover_hours
        else
          rollover_hours = @fusion_project.this_month_rollover_hours
        end
      end
      #puts  "[#{rollover_hours}]"
      # In the case, Finance Ticket has a collect rollover hours
      #rollover_hours = 0 unless rollover_hours.kind_of?(Integer)
      
      collection.finalilze rollover_hours
    end
  end


end



