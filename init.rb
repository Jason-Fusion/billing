require 'finance_listener'



Redmine::Plugin.register :billing do
  name 'Billing plugin'
  author 'Hidekazu Inoue'
  description 'This is a billing plugin for Fusion Systems'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  project_module :billing do
    permission :billing, { :billing => [:index] }, :public => true
    permission :invoice, { :invoice => [:index] }, :public => true
  end

  menu :project_menu, :billing, { :controller => 'billing', :action => 'index' }, :caption => 'Billing', :param => :project_id
  menu :project_menu, :invoice, { :controller => 'invoice', :action => 'index' }, :caption => 'Invoice', :param => :project_id

end

# This plugin has patch. Please refer to patch direcotry


